<?php

class GojekMethod_MyCarrier_Model_Carrier extends Mage_Shipping_Model_Carrier_Abstract implements Mage_Shipping_Model_Carrier_Interface {


    protected $_code = 'gojekmethod_mycarrier';
    const IND_COUNTRY_ID = 'IDN';
    protected $rateResultFactory;
    protected $rateMethodFactory;

    public function collectRates(Mage_Shipping_Model_Rate_Request $request) 
    {
        
        
		$hour    = Mage::getSingleton('core/date')->date('G');
        //$destination = '-6.203189349609291,106.64368629455566';
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {
            // $customer = Mage::getSingleton('customer/session')->getCustomer();
            // $destination = $customer->getPrimaryShippingAddress()->getData('vat_id');	


            $quote = Mage::getSingleton('checkout/session')->getQuote();
            $destination = $quote->getShippingAddress()->getData('vat_id');	





        }
        

        if($hour >= 6 &&  $hour <= 17 && $destination !== null) {
        $url    = 'https://integration-kilat-api.gojekapi.com:443/gokilat/v10/calculate/price?origin=-6.2586327,106.6123913&destination=' . $destination . '&paymentType=3';
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  		curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Client-ID: wearinasia-engine","Pass-Key: 22BA03D64908A0E20460702B8A46AE0EC5A6AC841943893A914CFAAB38AAA7B8"));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_GETFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
       
      
        curl_close($curl); 
	
		

        $result = Mage::getModel('shipping/rate_result');
        
        $expressWeightThreshold = $this->getConfigData('express_weight_threshold');

        $city = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getCity();    

        $priceI = $array['Instant']['price']['total_price'] ;
        $priceS = $array['SameDay']['price']['total_price'] ;
		
		$descinstant = $array['Instant']['shipment_method_description'] ;
		$descsameday = $array['SameDay']['shipment_method_description'] ;
		
        $restrictedArea = array('Tangerang', 'Jakarta', 'Bogor', 'Depok', 'Bekasi');
		foreach($restrictedArea as $value){
            if(strstr($city, $value) == true )
            {
                            if($priceI !== null){

                                $result->append($this->_getInstantShippingRate($priceI,$descinstant));	
                            }

                            if($priceS  !== null){

                                $result->append($this->_getSameDayShippingRate($priceS,$descsameday));
                            }
                            
                           
                    
            }
        }
        return $result;
    

        }
    
    	
    
        
       
    }


    protected function _getInstantShippingRate($priceI,$descinstant) {
        $rate = Mage::getModel('shipping/rate_result_method');
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod('priceInstant');
        $rate->setMethodTitle('GoSend Instant (By GoJek)');
        $rate->setMethodDescription('Estimasi pengiriman 6 - 9 Jam');
        $rate->setPrice($this->priceI = $priceI);
        $rate->setCost($this->priceI = $priceI);   
        return $rate;
    }

    protected function _getSameDayShippingRate($priceS,$descsameday) {
        $rate = Mage::getModel('shipping/rate_result_method');
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod('priceSameDay');
        $rate->setMethodTitle('GoSend Sameday (By GoJek)');
        $rate->setMethodDescription('Estimasi pengiriman 1 - 3 Jam');
        $rate->setPrice($this->priceS = $priceS);
        //$rate->setPrice(0);
        
        //$rate->setCost($this->priceS = $priceS);   
        $rate->setCost(0);   
        
        return $rate;
    }

    public function getAllowedMethods() {
        return array(
            'priceInstant' => 'Instant',
            'priceSameDay' => 'Same Day',
          
        );
    }

}
