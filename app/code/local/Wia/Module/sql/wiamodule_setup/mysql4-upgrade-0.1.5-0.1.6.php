<?php
echo 'Testing our upgrade script (0.1.6) and NOT halting execution <br />';

$installer = new Mage_Sales_Model_Mysql4_Setup('core_setup');
    
$installer->startSetup();
    


$to_time  = array(
    'type'          => 'text',
    'backend_type'  => 'text',
    'frontend_input' => 'text',
    'is_user_defined' => true,
    'label'         => 'to_time',
    'visible'       => true,
    'required'      => false,
    'user_defined'  => false,   
    'searchable'    => false,
    'filterable'    => false,
    'comparable'    => false,
    'default'       => ''
);
$installer->addAttribute('order', 'to_time', $to_time);
$installer->addAttribute('quote', 'to_time', $to_time);





$from_time  = array(
    'type'          => 'text',
    'backend_type'  => 'text',
    'frontend_input' => 'text',
    'is_user_defined' => true,
    'label'         => 'from_time',
    'visible'       => true,
    'required'      => false,
    'user_defined'  => false,   
    'searchable'    => false,
    'filterable'    => false,
    'comparable'    => false,
    'default'       => ''
);
$installer->addAttribute('order', 'from_time', $from_time);
$installer->addAttribute('quote', 'from_time', $from_time);


    $installer->endSetup();

?>