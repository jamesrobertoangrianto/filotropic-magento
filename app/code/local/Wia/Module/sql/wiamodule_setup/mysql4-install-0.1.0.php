<?php
echo 'Testing our upgrade script (1) and NOT halting execution <br />';

$installer = new Mage_Sales_Model_Mysql4_Setup('core_setup');
    $installer->startSetup();
    $attribute  = array(
            'type'          => 'text',
            'backend_type'  => 'text',
            'frontend_input' => 'text',
            'is_user_defined' => true,
            'label'         => 'business_id',
            'visible'       => true,
            'required'      => false,
            'user_defined'  => false,   
            'searchable'    => false,
            'filterable'    => false,
            'comparable'    => false,
            'default'       => ''
    );
    $installer->addAttribute('order', 'business_id', $attribute);
    $installer->addAttribute('quote', 'business_id', $attribute);
    $installer->endSetup();



?>