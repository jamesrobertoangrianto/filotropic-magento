<?php
echo 'Testing our upgrade script (0.1.5) and NOT halting execution <br />';


$installer = $this;
$installer->startSetup();
$table = $installer->getConnection()->newTable($installer->getTable('Module/gallery'))
    ->addColumn('gallery_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'identity' => true,
        ), 'gallery ID')

        ->addColumn('business_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
            ), 'gallery business_id')

        ->addColumn('image_path', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
        ), 'image_path url')

        ->addColumn('image_name', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
            ), 'image_name url')



        


        ->addColumn('timestamp', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'Timestamp')

    ->setComment('Magentotutorial weblog/blogpost entity table');
$installer->getConnection()->createTable($table);

$installer->endSetup();


?>
