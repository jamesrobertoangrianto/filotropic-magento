<?php
class Wia_Module_Checkoutv3Controller extends Mage_Core_Controller_Front_Action{

    public function indexAction(){
      echo '123';
    }

    public function _sendAPI($data){   
        $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
    
    }

    public function _getCustomerId(){
        $dev = $this->getRequest()->getParam('dev');
        if($dev == 1){
            $customer_id = 194;
        }
        else{
            $customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
        }
        return $customer_id;
    }

    public function _getCustomer(){   
        $customer_id = $this->_getCustomerId();
        $customer = Mage::getModel('customer/customer')->load($customer_id);

        return $customer;
    }

    protected function _clearsession(){
        // Clear Magento cart and quote items
        $cart = Mage::getModel('checkout/cart');                
        $cart->truncate()->save(); // remove all active items in cart page
        $cart->init();
        $session= Mage::getSingleton('checkout/session');
        $quote = $session->getQuote();
        $cart = Mage::getModel('checkout/cart');
        $cartItems = $cart->getItems();
        foreach ($cartItems as $item)
        {
            $quote->removeItem($item->getId())->save();
        }
        Mage::getSingleton('checkout/cart')->truncate();
        Mage::getSingleton('checkout/cart')->save();
        Mage::getSingleton('checkout/session')->clear();
        
    }

    public function _getInstallment($data){

        $installments = $data;

        $pieces[] = explode(",", $installments);

        foreach($pieces as $value){
            $arr = $value;
        }
        if ($arr = ["415","158","157","156"]){
            $installment = 24;
        }
        else if ($arr = ["158","157","156"]){
            $installment = 12;
        }
        else if(["158","157"]){
            $installment = 6;
        }
        else if(["158"]){
            $installment = 3;
        }
        else {}
        
        return $installment;
    }

    public function _getCart(){
        return Mage::helper('checkout/cart')->getCart();
    }
    
    public function _getQuote(){
        $dev = $this->getRequest()->getParam('dev');
        if($dev == 1){
            return Mage::getModel('sales/quote')->loadByCustomer($this->_getCustomer()); 
        }
        else{
            return Mage::getSingleton('checkout/cart')->getQuote();
        }
        
       
    }

    public function _getCartItems(){

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();
        $customer = $this->_getCustomer();
        $quote = $this->_getQuote();
        $count = 0;
         
        foreach ($quote->getAllItems() as $item) {
            if ($item->getParentItemId()) {
                continue;
            }
            $product = $item->getProduct();

            $productType = $product->getTypeId();
            if($productType  == 'configurable'){
                $product =  $item->getOptionByCode('simple_product')->getProduct();
            }

            foreach($product->getOptions() as $option) {
                $product_custom_options[$count][] = array(
                    'option_id' => $option->getId(),
                    'product_id' => $option->getProductId(),
                    'title' => $option->getTitle(),
                    'type' => $option->getType(),
                    'sku' => $option->getSku(),
                    'price' => $option->getPrice(),
                    'price_type' => $option->getPriceType()
                );
            } 
            // $options = Mage::helper('catalog/product_configuration')->getCustomOptions($item);

            $optionIds = $item->getOptionByCode('option_ids');
            if ($optionIds) {
                $options = array();
                foreach (explode(',', $optionIds->getValue()) as $optionId) {
                    $option = $product->getOptionById($optionId);
                    if ($option) {
                        $itemOption = $item->getOptionByCode('option_' . $option->getId());
                        $group = $option->groupFactory($option->getType())
                            ->setOption($option)
                            ->setConfigurationItem($item)
                            ->setConfigurationItemOption($itemOption);

                        if ('file' == $option->getType()) {
                            $downloadParams = $item->getFileDownloadParams();
                            if ($downloadParams) {
                                $url = $downloadParams->getUrl();
                                if ($url) {
                                    $group->setCustomOptionDownloadUrl($url);
                                }
                                $urlParams = $downloadParams->getUrlParams();
                                if ($urlParams) {
                                    $group->setCustomOptionUrlParams($urlParams);
                                }
                            }
                        }

                        $options[] = array(
                            'label' => $option->getTitle(),
                            'value' => $group->getFormattedOptionValue($itemOption->getValue()),
                            // 'print_value' => $group->getPrintableOptionValue($itemOption->getValue()),
                            'option_id' => $option->getId(),
                            'option_type' => $option->getType(),
                            'custom_view' => $group->isCustomizedView(),
                            'price' => $option->getPrice()
                        );
                    }
                }
            }
          
            $response[]= array (
                'item_id' => $item->getItemId(),
                'product_id' => $product->getId(),
                'name' =>  $product->getName(),
                'brand' => $product->getAttributeText("brand"),
                'request_quantity' => $item->getQty(),
                'available_quantity' => $product->getStockItem()->getQty() * 1,
                'url_path' => $item->getProduct()->getUrlPath(),
                'has_error' => $item->getHasError(),
                'price' => $item->getParentItemId()?$item->getParentItem()->getPrice(): $item->getPrice(),
                'featured_image' => array(
                    'image' => (string)Mage::helper('catalog/image')->init($item->getProduct(), 'small_image')->resize(100,100),
                    'image_2x' => (string)Mage::helper('catalog/image')->init($item->getProduct(), 'small_image')->resize(150,150),
                ),
                'installment_term' => $this->_getInstallment($product->getInstallment()),
                'product_custom_options_list' => $product_custom_options[$count],
                'custom_options' => $options,
            );
            $count = $count + 1;
        }
        return $response;

    }
   
    public function _CartSession(){       
    }

    public function _setAddressToCart($data){
        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();
        $customer = $this->_getCustomer();

        $customerShippingAddress = $data;
        // Update the cart's quote.
        $cart = Mage::getSingleton('checkout/cart');
        $address = $cart->getQuote()->getShippingAddress();

        $address->setCountryId($customerShippingAddress->getCountryId())
                ->setPostcode($customerShippingAddress->getPostcode())
                ->setCity($customerShippingAddress->getCity())
                ->setRegionId($customerShippingAddress->getRegionId())
                ->setRegion($customerShippingAddress->getRegion())
                ->setStreet(array(
                    '0' => $customerShippingAddress->getStreet(1), // compulsory
                    '1' => $customerShippingAddress->getStreet(2) // optional
                ))
                ->setTelephone($customerShippingAddress->getTelephone())
                ->setData('vat_id', $customerShippingAddress->getData('vat_id'))
                ->setFirstname($customer->getFirstname())
                ->setLastname($customer->getLastname())
                ->setCollectShippingrates(true);
                $address->save();
                $cart->save();
    }

    public function AddressAction(){
        // $customer = Mage::getSingleton('customer/session')->getCustomer();
        $customer = $this->_getCustomer();

        $params = $this->getRequest()->getParams();

        $addressId = $params['id'];
        $show = $params['show'];

        if ($_SERVER['REQUEST_METHOD'] === 'GET'){
            if($show == 1){
                $this->_getAddress($customer);
            }
            else {
                $this->_getAddressList($customer);
            }
        }
        else if ($_SERVER['REQUEST_METHOD'] === 'POST'){
            if(!empty($addressId)){
                $this->_setDefault($customer, $addressId);
            }
            else {
                $this->_createNewAddress($customer);
            }
        }
        else if ($_SERVER['REQUEST_METHOD'] === 'PUT'){
            $this->_editAddressbyAddressId($customer, $addressId);
        }
        else {
            $message_code = array('message_code' => 405, 'message_dialog' => 'Method Not Allowed');
            $this->_sendAPI($message_code);
        }
    }

    public function _getAddressList($data){
        $customer = $data;
        
        foreach ($customer->getAddresses() as $address){
        //$data = $address->toArray();
            $customeraddress[] = array(
                
                'id' => $address->getId(),
                'customer_id' => $customer->getId(),
                'firstname' => $customer->getFirstname(),
                'lastname' => $customer->getLastname(),
                'email' => $customer->getEmail(),
                'company' => $address->getCompany(),
                'city' => $address->getCity(),
                'region' => $address->getRegion(),
                'postcode' => $address->getPostcode(),
                'country_id' => $address->getCountryId(),
                'telephone' => $address->getTelephone(),
                'region_id' => $address->getRegionId(),
                'street' => $address->getStreet(1),
                'location' => $address->getStreet(2),
                'vat_id' => $address->getData('vat_id')
            );
        }

        if(!empty($customeraddress)){
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Get Address List success',
                'address_list' => $customeraddress
            );
        }
        else {
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Address Not Found',
                'address_list' => $customeraddress
            );
        }

        $this->_sendAPI($message_code);
    }

    public function _getAddress($data){
        $customer = $data;
        $address = $customer->getPrimaryShippingAddress();

        if(!empty($address)){
            $customeraddress = array(
                'id' => $address->getId(),
                'customer_id' => $customer->getId(),
                'firstname' => $customer->getFirstname(),
                'lastname' => $customer->getLastname(),
                'email' => $customer->getEmail(),
                'company' => $address->getCompany(),
                'city' => $address->getCity(),
                'region' => $address->getRegion(),
                'postcode' => $address->getPostcode(),
                'country_id' => $address->getCountryId(),
                'telephone' => $address->getTelephone(),
                'region_id' => $address->getRegionId(),
                'street' => $address->getStreet(1),
                'location' => $address->getStreet(2),
                'vat_id' => $address->getData('vat_id')
            );
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Get Address Success',
                'address' => $customeraddress
            );
        }
        else {
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Address not found',
                'address' => $customeraddress
            );
        }

        $this->_sendAPI($message_code);
    }

    public function _editAddressbyAddressId($data, $data1){
        $customer = $data;
        $addressId = $data1;
        // $address = $customer->getPrimaryShippingAddress();
        $address = Mage::getModel("customer/address")->load($addressId);

        $getraws = $this->getRequest()->getRawBody();
        $params = json_decode($getraws, true);

        // $customerFirstname = $params['firstname'];
        // $customerLastname = $params['lastname'];
        $customerCompany = $params['company'];
        $customerTelephone = $params['telephone'];
        $customerCity = $params['city'];
        $customerRegion = $params['region'];
        $customerRegionId = $params['region_id'];
        $customerPostalcode = $params['postcode'];
        $customerCountryId = $params['country_id'];   
        $customerStreet = $params['street'];
        $location = $params['location'];
        $vat = $params['vat_id']; 

        $street = array(
            '0' => $customerStreet, // compulsory
            '1' => $location // optional
        );

        $address->setCountryId($customerCountryId);
        $address->setPostcode($customerPostalcode);
        $address->setCity($customerCity);
        $address->setTelephone($customerTelephone);
        $address->setCompany($customerCompany);
        $address->setStreet($street);
        $address->setRegion($customerRegion);
        $address->setRegionId($customerRegionId);
        $address->setData('vat_id', $vat);
        // $address->setData();
        // $address->setStreet(1, $customerStreet);
        // $address->setStreet(2, $location);
        $address->save();

        $this->_setAddressToCart($address);

        $customeraddress = array(
            'id' => $address->getId(),
            'customer_id' => $customer->getId(),
            'firstname' => $customer->getFirstname(),
            'lastname' => $customer->getLastname(),
            'email' => $customer->getEmail(),
            'company' => $address->getCompany(),
            'city' => $address->getCity(),
            'region' => $address->getRegion(),
            'postcode' => $address->getPostcode(),
            'country_id' => $address->getCountryId(),
            'telephone' => $address->getTelephone(),
            'region_id' => $address->getRegionId(),
            'street' => $address->getStreet(1),
            'location' => $address->getStreet(2),
            'vat_id' => $address->getData('vat_id')
        );

        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'Address Edited',
            'address' => $customeraddress
        );

        $this->_sendAPI($message_code);
    }

    public function _setDefault($data, $data1){
        $customer = $data;
        $addressId = $data1;
        // $address = $customer->getPrimaryShippingAddress();
        $address = Mage::getModel("customer/address")->load($addressId);

        $address->setIsDefaultBilling('1');
        $address->setIsDefaultShipping('1');
        $address->save();

        $this->_setAddressToCart($address);

        $cart = Mage::getSingleton('checkout/cart');
        $address = $cart->getQuote()->getShippingAddress();

        $customeraddress = array(
            'id' => $address->getId(),
            'customer_id' => $customer->getId(),
            'firstname' => $customer->getFirstname(),
            'lastname' => $customer->getLastname(),
            'email' => $customer->getEmail(),
            'company' => $address->getCompany(),
            'city' => $address->getCity(),
            'region' => $address->getRegion(),
            'postcode' => $address->getPostcode(),
            'country_id' => $address->getCountryId(),
            'telephone' => $address->getTelephone(),
            'region_id' => $address->getRegionId(),
            'street' => $address->getStreet(1),
            'location' => $address->getStreet(2),
            'vat_id' => $address->getData('vat_id')
        );
        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'Address has been set to default.',
            'address' => $customeraddress
        );

        $this->_sendAPI($message_code);
    }
   
    public function _createNewAddress($data){
            
            // if (!$this->_validateFormKey()) {
            //     $this->_redirect('*/*');
            //     return;
            // }

            //login customer data
            $websiteId = Mage::app()->getWebsite()->getId();
            $store = Mage::app()->getStore();
            $customer = $data;
        
            $getraws = $this->getRequest()->getRawBody();
            $params = json_decode($getraws, true);

            //var_dump($params);
            $firstName = $customer->getFirstname();
            $lastName = $customer->getLastname();
            $region = $params['region'];
            $regionid = $params['region_id'];
            $code = $params['postcode'];
            $city = $params['city'];
            $street = $params['street'];
            $location = $params['location'];
            $phone = $params['telephone'];
            $vat = $params['vat_id'];

            $shippingAddress = array(
            'firstname' => $firstName,
            'middlename' => '',
            'lastname' => $lastName,
            'street' => array(
                '0' => $street, // compulsory
                '1' => $location // optional
            ),
            'city' => $city,
            'country_id' => 'ID', // two letters country code
            'region' => $region, // can be empty '' if no region
            'region_id' => $regionid, // can be empty '' if no region_id
            'postcode' => $code,
            'telephone' => $phone,
            'fax' => '',
            'vat_id' => $vat,
            'save_in_address_book' => 1
        );

        $address = Mage::getModel('customer/address');  
        $address->setData($shippingAddress)
            ->setCustomerId($customer->getId())
            ->setIsDefaultBilling('1')
            ->setIsDefaultShipping('1')
            ->setSaveInAddressBook('1');
        $address->save();

        $this->_setAddressToCart($address);

        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'Address successfully created.',
            'id' => $address->getId()
        );

        $this->_sendAPI($message_code);
        // $this->_redirect('checkout/onepage');
    }

    protected function _initProduct()
    {
        $productId = (int) $this->getRequest()->getParam('product');
        if ($productId) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($productId);
            if ($product->getId()) {
                return $product;
            }
        }
        return false;
    }

    //1. ADD TO CART
    public function addtoCartAction(){
        try {
            
            $cart = Mage::getModel('checkout/cart');
            $product = $this->_initProduct();


            //$params = $this->getRequest()->getParams();

            $getraws = $this->getRequest()->getRawBody();
            $params = json_decode($getraws, true);

            $cart->init();
            $cart->addProduct($product, $params);
            $cart->save();
            Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
            $qty = Mage::helper('checkout/cart')->getSummaryCount();
            $price = Mage::helper('checkout/cart')->getQuote()->getSubtotal();
            //$formated_price = Mage::helper('core')->formatPrice($price, true);

            $response = array( 
                'status_code' => 200,
                'message_dialog' => 'All is good',
                'qty' => $qty,
                'items' => $this->_getCartItems(),
                'subtotal' =>$price
                
            );

        } catch (Exception $e) {
            $response = array( 
                'status_code' => 400,
                'message_dialog' => $e->getMessage(),   
                'qty' => $qty,
                    'items' => $this->_getCartItems(),
                    'subtotal' =>$price
            );
        }
        $this->_sendAPI( $response);
    }

    //2. CART
    public function CartAction(){

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();
        $customer = $this->_getCustomer();
        $quote = $this->_getQuote();
        $quote->getAllItems();
        $quote->save();
        
      
        $subtotal = $quote->getSubtotal();
        $grand = $quote->getGrandTotal();
        

        if($this->_getCartItems() == null) {
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Cart null'
            );
        }
        else {
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Get Cart Success',
                'id' => $quote->getId(),
                'quote' => $this->_getCartItems(),
                'total_items' => $this->_getCart()->getItemsCount(),
                'subtotal' => $subtotal,
                'total' => $grand
            );
        }
        $this->_sendAPI($message_code);
    }

    public function CartQtyAction(){
        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'Get Cart Success',
            'total_items' =>  Mage::helper('checkout/cart')->getSummaryCount(),
            
           
        );
        $this->_sendAPI($message_code);
    }

    public function UpdateCartCustomOptionsAction(){
        $params = $this->getRequest()->getParams();
        $option_id = $params['option_id'];
        $value = $params['value'];
        $id = (int)$this->getRequest()->getParam('item_id');
                
        // $items = $cartHelper->getCart()->getItems();
        //$cart = Mage::getSingleton('checkout/cart');

        $cart = Mage::getSingleton('checkout/cart');
        $options = array('92'=>'49','144'=>'21');
        $item = Mage::getSingleton('checkout/cart')->getQuote()->getItemById($id);
        // $items = $cartHelper->getCart()->getItems();
        
        $paramater = array('product' => $item->getProductId(),
                            'qty' => $item->getQty(),
                            'form_key' => Mage::getSingleton('core/session')->getFormKey(),
                            'options' => array($option_id => $value)
                    );
        try {
            $request = new Varien_Object();
            $request->setData($paramater);
            $cart->updateItem($item->getItemId(), $request);
            $cart->save();
            Mage::getSingleton('checkout/session')->setCartWasUpdated(true);

            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Custom Options successfully updated'
            );
            Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
        } catch (Mage_Core_Exception $e) {
            //$redirecturl = Mage::getBaseUrl().'snapbarugopay/payment/redirect';
            $result['success'] = false;
            $result['error'] = true;
            $result['error_messages'] = $e->getMessage();
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Order cannot be create, '.$e->getMessage()
            );
            Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
             
            if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                Mage::getSingleton('checkout/session')->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    Mage::getSingleton('checkout/session')->addError(Mage::helper('core')->escapeHtml($message));
                }
            }
        } 
        catch (Exception $e) {
            $result['success']  = false;
            $result['error']    = true;
            $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
            Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
             
            Mage::logException($e);
            //$this->_goBack();
        } 

        
    }

    //3. CHECKOUT
    public function QuoteAction(){
        // if (!Mage::getSingleton('customer/session')->isLoggedIn()){
        //     $message_code = array( 
        //         'message_code' => 300,
        //         'message_dialog' => 'Not Login',
        //     );
        //     $this->_sendAPI($message_code);
        //     return;
        // }
        
        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();
        $customer = $this->_getCustomer();
        $quote = $this->_getQuote();
        $address = $quote->getShippingAddress();
    
        //check if shipping address is set
        if($quote->getShippingAddress()->getRegionId() == null){
            //$getaddress = $customer->getPrimaryShippingAddress();
            if($customer->getPrimaryShippingAddress() == null){
                $customeraddress == null;
            }
            else {
                $this->_setAddressToCart($customer->getPrimaryShippingAddress());
                // $quote = Mage::getModel("checkout/session")->getQuote();
                // $address = $quote->getShippingAddress();
                $shippingaddress = array(
                    'id' => $address->getAddressId(),
                    'customer_id' => $customer->getId(),
                    'firstname' => $address->getFirstname(),
                    'lastname' => $address->getLastname(),
                    'email' => $customer->getEmail(),
                    'company' => $address->getCompany(),
                    'city' => $address->getCity(),
                    'region' => $address->getRegion(),
                    'postcode' => $address->getPostcode(),
                    'country_id' => $address->getCountryId(),
                    'telephone' => $address->getTelephone(),
                    'region_id' => $address->getRegionId(),
                    'street' => $address->getStreet(1),
                    'location' => $address->getStreet(2),
                    'vat_id' => $address->getData('vat_id')
                );
                
                // foreach($address->getAllShippingRates() as $rate){
                //     if($rate->getCode() == 'flatrate_flatrate'){}
                //     else {
                //         $array[] = array(
                //             'carrier' => $rate->getCarrier(),
                //             'carrier_title' => $rate->getCarrierTitle(),
                //             'code' => $rate->getCode(),
                //             'method' => $rate->getMethod(),
                //             'method_description' => $rate->getMethodDescription(),
                //             'price' => $rate->getPrice(),
                //             'error_message' => $rate->getErrorMessage(),
                //             'method_title' => $rate->getMethodTitle(),
                //             'carrierName' => $rate->getCarrierName()
                //         );
                //     }
                // }
            }
        }
        else {
            if($address->getShippingAmount() == 0){
                $this->_setAddressToCart($address);
            }
      
            $shippingaddress = array(
                'id' => $address->getAddressId(),
                'customer_id' => $customer->getId(),
                'firstname' => $address->getFirstname(),
                'lastname' => $address->getLastname(),
                'email' => $customer->getEmail(),
                'company' => $address->getCompany(),
                'city' => $address->getCity(),
                'region' => $address->getRegion(),
                'postcode' => $address->getPostcode(),
                'country_id' => $address->getCountryId(),
                'telephone' => $address->getTelephone(),
                'region_id' => $address->getRegionId(),
                'street' => $address->getStreet(1),
                'location' => $address->getStreet(2),
                'vat_id' => $address->getData('vat_id')
            );
        }

        $customerarray = array(
            'entity_id' => $customer->getId(),
            'increment_id' => $customer->getIncrementId(),
            'firstname' => $customer->getFirstname(),
            'lastname' => $customer->getLastname(),
            'email' => $customer->getEmail(),
            'rating' => $customer->getRating(),
            'verified' => $customer->getVerified(),

        );

        foreach($address->getAllShippingRates() as $rate){
            if($rate->getCode() == 'flatrate_flatrate'){
                continue;
            }
            else {
                $array[] = array(    
                    'code' => $rate->getCode(),
                    'method' => $rate->getMethod(),
                    'method_description' => $rate->getMethodDescription(),
                    'price' => $rate->getPrice(),
                   
                );
            }
        }

        $shipping = $quote->getShippingAddress()->getShippingAmount();
        $subtotal = $quote->getSubtotal();
        $grand = $quote->getGrandTotal();
        
        if($this->_getCartItems() == null) {
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Quotes null'
            );
        }
        else {
            
            if($quote->getShippingAddress()->getShippingMethod() == !null && $shippingaddress == !null){
                $validation = true;
            }
            else{
                $validation = false; 
            }
            
            $message_code = array( 
                'allow_to_checkout' => $validation,
                'message_code' => 200,
                'message_dialog' => 'Get Quotes Success',
                'id' => $quote->getId(),
                'customer' =>  $customerarray,
                'shipping_address' => $shippingaddress,
                'total_items' =>  Mage::helper('checkout/cart')->getSummaryCount(),
                'quote' => $this->_getCartItems(),
                'selected_shipping_code' => $quote->getShippingAddress()->getShippingMethod(),
                'shipping_list' =>  $array,
                'promo' => $subtotal - $grand + $shipping,
                'coupon' => $quote->getCouponCode(),
                'applied_rule_ids' => $quote->getAppliedRuleIds(),
                'shipping_cost' => $shipping,
                'subtotal' => $subtotal,
                'total' => $grand,
               
            );
        }
        
        $this->_sendAPI($message_code);
    }

    public function createOrderAction(){
        
        // if (!$this->_validateFormKey()) {
        //     $this->_redirect('/success');
        //     return;
        // }

        $getraws = $this->getRequest()->getRawBody();
        $params = json_decode($getraws, true);

        $recepient = $params['recepient'];
        $message = $params['message'];

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

        $customer = $customer = $this->_getCustomer();
        // $formKeys = Mage::getSingleton('core/session')->getFormKey();
        
        $paymentMethod = 'snap';
        // $paymentMethod = 'banktransfer';

        $shippingMethod =  Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingMethod();
        // $shippingMethod = 'flatrate_flatrate';

        $quote = Mage::helper('checkout/cart')->getCart()->getQuote();
        // $quote = Mage::getModel('sales/quote')->load($quoteId);
        $quote->getAllItems();

        // $shippingMethod = $quote->getShippingMethod();

        // Set currency for the quote
        $quote->setCurrency(Mage::app()->getStore()->getBaseCurrencyCode()); 

        // Assign customer to quote
        $quote->assignCustomer($customer);

        $billingAddressData = $quote->getBillingAddress();//->addData($customerBillingAddress);
        $shippingAddressData = $quote->getShippingAddress();//->addData($customerShippingAddress);

        // Set shipping and payment method on quote shipping address data
        $shippingAddressData->setShippingMethod($shippingMethod)
        ->setPaymentMethod($paymentMethod);

        // Set payment method for the quote
        $quote->getPayment()->importData(array('method' => $paymentMethod));


        try {
            // Collect totals of the quote
            $quote->collectTotals();
            
            // Save quote
            $quote->save();
                
            // Create Order From Quote
            $service = Mage::getModel('sales/service_quote', $quote);
            $service->submitAll();
            $incrementId = $service->getOrder()->getRealOrderId();
            $orderId = $service->getOrder()->getId();
                

            Mage::getSingleton('checkout/session')
            //    ->setQuoteId('123')
            //    ->setLastRealOrderId('123');
                ->setRealOrderId($incrementId)
                ->setLastRealOrderId($incrementId);


            //set comment
            $order = Mage::getModel('sales/order')->load($orderId);
            //  $order->sendNewOrderEmail();
            //  $order->addStatusHistoryComment('Booking Date : '.$date.'</br>'.'Booking Time '.$time);
            $order->addStatusHistoryComment(Mage::helper('core')->jsonEncode(Mage::helper('core/http')->getHttpUserAgent()));
            $order->addStatusHistoryComment('Order dari Mobile');
            
            $order->save();

            if(!empty($recepient) && !empty($message)){
                $gift = $this->_gift($order, $recepient, $message);
            }
            else {}

            $result['success'] = true;
            $result['error'] = false;
            
            // $redirecturl = Mage::getBaseUrl().'snapio/payment/redirect';
            
            //$response['response'] = $this->myAjax();
            // $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            // $this->getResponse()->setRedirect($redirecturl);
            // $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result)); 

            $result = Mage::helper('Wia_Module_helper')->Payment($incrementId);

            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Order has been created',
                'order_id' => $order->getId(),
                'payment' => $result,
                'gift_message' => $gift
            );
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
            $subscriber = Mage::getModel('advancednewsletter/subscriber')->subscribe($customer->getEmail(), 'new_buyer_customer','');
            $this->_clearsession();

            // $this->_sendAPI($message_code); 
                
            } catch (Mage_Core_Exception $e) {
                //$redirecturl = Mage::getBaseUrl().'snapbarugopay/payment/redirect';
                $result['success'] = false;
                $result['error'] = true;
                $result['error_messages'] = $e->getMessage();
                $message_code = array( 
                    'message_code' => 400,
                    'message_dialog' => 'Order cannot be create, '.$e->getMessage()
                );
                Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
                
                if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                    Mage::getSingleton('checkout/session')->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    foreach ($messages as $message) {
                        Mage::getSingleton('checkout/session')->addError(Mage::helper('core')->escapeHtml($message));
                    }
                }
            } 
            catch (Exception $e) {
                $result['success']  = false;
                $result['error']    = true;
                $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
                Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                
                Mage::logException($e);
                //$this->_goBack();
            } 
    }

    public function PayAction(){
        $params = $this->getRequest()->getParams();
        $orderIncrementId = $params['order_id'];

        $result = Mage::helper('Wia_Module_helper')->Payment($orderIncrementId);
        
        if(!empty($result['token'])){
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Token found',
                'order_id' => $orderIncrementId,
                'payment' => $result
            );
        }
        else{
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Token not found',
                'order_id' => $orderIncrementId,
                'payment' => $result
            );
        }
        
        
        $this->_sendAPI($message_code);
    }

    public function ShippingListAction(){

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

        $customer = $this->_getCustomer();
        $customerShippingAddress = $customer->getPrimaryShippingAddress();
        // Update the cart's quote.
        $cart = Mage::getSingleton('checkout/cart');
        $address = $cart->getQuote()->getShippingAddress();


        $address->setCountryId($customerShippingAddress->getCountryId())
                ->setPostcode($customerShippingAddress->getPostcode())
                ->setCity($customerShippingAddress->getCity())
                ->setRegionId($customerShippingAddress->getRegionId())
                ->setRegion($customerShippingAddress->getRegion())
                ->setCollectShippingrates(true);
        $cart->save();

        // Find if our shipping has been included.
        $rates = $address->collectShippingRates()
        ->getGroupedAllShippingRates();

        $shipping = $address->collectShippingRates()->getGroupedAllShippingRates();

        if($shipping == null){
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Shipping List Null'
            );
        }
        else {
            foreach($shipping as $methods => $rates){
                foreach ($rates as $rate){
                    if($rate['code'] == 'flatrate_flatrate'){}
                    else {
                        $array[] = array(
                            'carrier' => $rate['carrier'],
                            'carrier_title' => $rate['carrier_title'],
                            'code' => $rate['code'],
                            'method' => $rate['method'],
                            'method_description' => $rate['method_description'],
                            'price' => $rate['price'],
                            'error_message' => $rate['error_message'],
                            'method_title' => $rate['method_title'],
                            'carrierName' => $rate['carrierName']
                        );
                    }
                }
            }
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'GET Shipping List success',
                'id' => $cart->getQuote()->getId(),
                'shipping' => $array
            );
        }
        $this->_sendAPI($message_code); 
    }

    public function SetShippingAction(){

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

        $params = $this->getRequest()->getParams();
        $code = $params['code'];

        $cart = Mage::getSingleton('checkout/cart');

        $address = $cart->getQuote()->getShippingAddress();
        $address->setShippingMethod($code)
                ->setCollectShippingrates(true);
        $cart->save();

        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'Set Shipping Method Success',
            'shipping' => Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingMethod()
        );

        $this->_sendAPI($message_code);
    }

    public function GetVoucherAction(){
        if ($_SERVER['REQUEST_METHOD'] === 'GET'){
            $coll = Mage::getResourceModel('salesrule/rule_collection')
            ->addFieldToFilter('is_rss', 1)
            ->load();
            
            $items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
            $customer = $this->_getCustomer();
            $quote = $this->_getQuote();
            $items = $quote->getAllItems();
            foreach($items as $item){ 
                foreach($coll as $rule) {
                     if ($rule->getActions()->validate($item)){
                        $promo = Mage::getModel('salesrule/rule')->load($rule->getId());
                        if(!empty($promo->getCouponCode())){
                            $array[] = array(
                                'rule_id' => $promo->getId(),
                                'coupon' => $promo->getCouponCode(),
                                'description' => $promo->getDescription(),
                                'coupon_amount' => $promo->getDiscountAmount()
                            );
                        } 
                    }
                }
            }
           
            // $test = $this->unique_multidim_array($array, 'code');
            // if(!empty($test)){
            //     $message_code = array( 
            //         'message_code' => 200,
            //         'message_dialog' => 'Get Promo Codes Success',
            //         'promo' => $test
            //     );
            // }
            // else {
            //     $message_code = array( 
            //         'message_code' => 400,
            //         'message_dialog' => 'No Promo Code',
            //         'promo' => $test
            //     );
            }
            $this->_sendAPI($array);
        
    }

    public function SetVoucherAction(){
        
        $params = $this->getRequest()->getParams();
        $code = $params['id'];
        $quote = $this->_getQuote();

        Mage::getSingleton('checkout/cart')->getQuote()
        ->setCouponCode($code)->collectTotals()->save();

        $quoteCouponCode = $quote->getCouponCode();
        $shipping = $quote->getShippingAddress()->getShippingAmount();
        $subtotal = $quote->getSubtotal();
        $grand = $quote->getGrandTotal();
        $promo = $subtotal - $grand + $shipping;
        if ($quoteCouponCode == $code){
            //$rule is valid and $rule->getCode() is a valid coupon code
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Set Promo Codes Success',
                'promo' => $quoteCouponCode,
                'discount_amount' => $promo
            );
        }
        else {
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Fail to Set Promo Codes Success',
                'promo' => $quoteCouponCode,
                'discount_amount' => $promo
            );
        }
            
        $this->_sendAPI($message_code);
    }

    public function CityListAction(){
        $params = $this->getRequest()->getParams();
        $region = $params['region'];
        
        $url    = 'https://wia.id/aongkir/index/city?prov_id='.$region;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);        
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        // curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
            $result     = curl_exec($curl);
            $array = Mage::helper('core')->jsondecode($result);
            curl_close($curl);

        if($array == null) {
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'City List is null'
            );
        }
        else {
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Get City List Success',
                'city_list' => $array
            );
        }
        $this->_sendAPI($message_code);
    }

    public function RegionListAction(){
        $params = $this->getRequest()->getParams();
        $country = $params['country'];
        $regions = Mage::getModel('directory/region')->getResourceCollection()
                ->addCountryFilter($country)
                ->load();

        foreach($regions as $regionlist){
            $array[] = array (
                'region_id' => $regionlist->getRegionId(),
                'country_id' => $regionlist->getCountryId(),
                'code' => $regionlist->getCode(),
                'default_name' => $regionlist->getDefaultName(),
                'name' => $regionlist->getName()
            );
        }
        
        if($array == null) {
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Region List is null'
            );
        }
        else {
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Get Region List Success',
                'region_list' => $array
            );
        }
        $this->_sendAPI($message_code);
    }

    public function DeleteProductAction(){

        $id = (int)$this->getRequest()->getParam('item_id');
        if ($id) {
            try {
                $cartHelper = Mage::helper('checkout/cart');

                $items = $cartHelper->getCart()->getItems();
                
                $cartHelper->getCart()->removeItem( $id )->save();
                Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
                $price = Mage::helper('checkout/cart')->getQuote()->getSubtotal();

                $message_code = array( 
                    'message_code' => 200,
                    'message_dialog' => 'Remove Item Success',
                    'items' => $this->_getCartItems(),
                    'subtotal' =>$price
                );
                $this->_sendAPI($message_code);
            } catch (Exception $e) {
                $message_code = array( 
                    'message_code' => 400,
                    'message_dialog' => 'Cannot Remove Item',
                    'items' => $this->_getCartItems(),
                    'subtotal' =>$price
                );
                $this->_sendAPI($message_code);
            }
        }
    }

    public function UpdateCartAction(){

        $id = (int)$this->getRequest()->getParam('item_id');
        $qty = $this->getRequest()->getParam('qty');
        
        if ($id) {
            try {
                $cart = $this->_getCart();
                $item = $this->_getQuote()->getItemById($id);

                $item->setQty($qty);
                $item->save();   

                $cart->save();


                $qty = Mage::helper('checkout/cart')->getSummaryCount();
                $price = Mage::helper('checkout/cart')->getQuote()->getSubtotal();

                
                $message_code = array( 
                    'message_code' => 200,
                    'message_dialog' => 'Update Cart Success',
                    'qty' => $qty,
                    'items' => $this->_getCartItems(),
                    'subtotal' =>$price
                );
                $this->_sendAPI($message_code);
            } catch (Exception $e) {
                $message_code = array( 
                    'message_code' => 400,
                    'message_dialog' => 'Cannot Update Cart',
                    'qty' => $qty,
                    'items' => $this->_getCartItems(),
                    'subtotal' =>$price
                );
                $this->_sendAPI($message_code);
            }
        }

    }

    public function completeOrderAction(){

        
        $order = $this->getRequest()->getParam('order_id');
        
        if ($order) {
            try {
                $order = Mage::getModel("sales/order")->loadByIncrementId($order);
                $order->setStatus("complete");
                $order->addStatusHistoryComment('Update status dari customer. Sudah terima pesanan', false);
                //$history->setIsCustomerNotified(false);
                $order->save();

                $message_code = array( 
                    'message_code' => 200,
                    'message_dialog' => 'Update Cart Success'
                );
                $this->_sendAPI($message_code);
            } catch (Exception $e) {
                $message_code = array( 
                    'message_code' => 400,
                    'message_dialog' => 'Cannot Update Cart'
                );
                $this->_sendAPI($message_code);
            }
        }

    }

    public function ReOrderAction(){

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

        try {
            $arrParams = (int)$this->getRequest()->getParam('order_id');
            if($arrParams) {
                $orders =  Mage::getModel('sales/order')->loadByIncrementId($arrParams);
                if (!$orders->getId()) {
                    Mage::getSingleton('adminhtml/session')->addError('Invalid order id'.$orderId);
                    echo 'Order Not Found';
                    exit;
                }
                else {
                    $shippingmethod = $orders->getShippingMethod();

                    $cart = Mage::getSingleton('checkout/cart');
                    $cart->init();
                    $options = array('92'=>'49','144'=>'21');

                    foreach ($orders->getAllItems() as $item) {
                        if ($item->getParentItemId()) {
                            continue;
                        }
                        
                        $product = $item->getProduct();
                        $paramater = array('product' => $item->getProductId(),
                                            'qty' => $item->getQtyOrdered(),
                                            'form_key' => Mage::getSingleton('core/session')->getFormKey(),
                                            'options' => array('option_id'=>'sub_option_id')
                                    );
                        // $orderedqty[] = $item->getQtyOrdered();
                        // $stockitem[] = $product->getStockItem()->getQty() * 1;
                        $request = new Varien_Object();
                        $request->setData($paramater);
                        try {
                            $cart->addProduct($product, $request);
                        } 
                        catch (Mage_Core_Exception $e) {
                            //$redirecturl = Mage::getBaseUrl().'snapbarugopay/payment/redirect';
                            $result['success'] = false;
                            $result['error'] = true;
                            $result['error_messages'] = $e->getMessage();
                            $message_code = array( 
                                'message_code' => 400,
                                'message_dialog' => 'Order cannot be create, '.$e->getMessage()
                            );
                            Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
                            
                            if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                                Mage::getSingleton('checkout/session')->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
                            } else {
                                $messages = array_unique(explode("\n", $e->getMessage()));
                                foreach ($messages as $message) {
                                    Mage::getSingleton('checkout/session')->addError(Mage::helper('core')->escapeHtml($message));
                                }
                            }
                        } 
                        catch (Exception $e) {
                            $result['success']  = false;
                            $result['error']    = true;
                            $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
                            Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                                
                            Mage::logException($e);
                            //$this->_goBack();
                        } 
                    }

                    // foreach($orderedqty as $ordered){
                        // if($ordered > $stockitem[$count]){
                        //     $message_code = array( 
                        //         'message_code' => 400,
                        //         'message_dialog' => 'One of the products quantity order exceed stock or out of stock'
                        //     );
                        //     $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
                        //     break;
                        // }
                        // else {
                            $cart->save();

                            $paymentMethod = 'snapio';
                            // $paymentMethod = 'banktransfer';

                            $quote = Mage::helper('checkout/cart')->getCart()->getQuote();
                            $quote->setCurrency(Mage::app()->getStore()->getBaseCurrencyCode());

                            $customer = $this->_getCustomer();
                            $quote->assignCustomer($customer);

                            $billingAddressData = $quote->getBillingAddress();//->addData($customerBillingAddress);
                            $shippingAddressData = $quote->getShippingAddress();//->addData($customerShippingAddress);
                            $shippingAddressData->setShippingMethod($shippingmethod)->setPaymentMethod($paymentMethod);

                            // Set payment method for the quote
                            $quote->getPayment()->importData(array('method' => $paymentMethod));

                            try {
                                // Collect totals of the quote
                                // $quote->getShippingAddress()->collectTotals();
                                $quote->collectTotals();
                                $quote->getShippingAddress()
                                    ->setCollectShippingRates(true)
                                    ->collectShippingRates()
                                    ->setShippingMethod($shippingmethod);

                                // Save quote
                                $quote->save();

                                // Create Order From Quote
                                $service = Mage::getModel('sales/service_quote', $quote);
                                $service->submitAll();
                                $incrementId = $service->getOrder()->getRealOrderId();
                                $orderId = $service->getOrder()->getId();

                                Mage::getSingleton('checkout/session')
                                //    ->setQuoteId('123')
                                //    ->setLastRealOrderId('123');
                                    ->setRealOrderId($incrementId)
                                    ->setLastRealOrderId($incrementId);
                                
                                //set comment
                                $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
                                //  $order->sendNewOrderEmail();
                                //  $order->addStatusHistoryComment('Booking Date : '.$date.'</br>'.'Booking Time '.$time);
                                $order->save();
                                $this->_clearsession();
                    
                                $result['success'] = true;
                                $result['error'] = false;
                    
                                $result = Mage::helper('Wia_Module_helper')->Payment($incrementId);
                                // $result = Mage::helper('Wia_Module_helper')->TestPayment($incrementId);
                    
                                $message_code = array( 
                                    'message_code' => 200,
                                    'message_dialog' => 'Order has been created',
                                    'order_id' => $order->getId(),
                                    'payment' => $result,
                                    'gift_message' => $gift
                                );
                                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
                                $this->_clearsession();
                        
                            //     $this->_sendAPI($message_code); 
                            } 
                            catch (Mage_Core_Exception $e) {
                                //$redirecturl = Mage::getBaseUrl().'snapbarugopay/payment/redirect';
                                $result['success'] = false;
                                $result['error'] = true;
                                $result['error_messages'] = $e->getMessage();
                                $message_code = array( 
                                    'message_code' => 400,
                                    'message_dialog' => 'Order cannot be create, '.$e->getMessage()
                                );
                                Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
                                
                                if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                                    Mage::getSingleton('checkout/session')->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
                                } else {
                                    $messages = array_unique(explode("\n", $e->getMessage()));
                                    foreach ($messages as $message) {
                                        Mage::getSingleton('checkout/session')->addError(Mage::helper('core')->escapeHtml($message));
                                    }
                                }
                            } 
                            catch (Exception $e) {
                                $result['success']  = false;
                                $result['error']    = true;
                                $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
                                Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                                    
                                Mage::logException($e);
                                //$this->_goBack();
                            } 
                        // }
                        // $count = $count + 1;
                    // }
                }
            }
        }
        catch(Exeption $e) {
            echo $e->getMessage();
        }
    }

    public function manualOrderstep1Action(){
        $getraws = $this->getRequest()->getRawBody();
        $params = json_decode($getraws, true);

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

        //Create sales quote object
        $quote = Mage::getModel('sales/quote')->setStoreId($store->getStoreId());
            
        $newcustomerflag = $params['newcustomerflag'];
        // $productid = $params['product_id'];
        // $productQty = $params['request_quantity'];
        $customeremail = $params['customer']['email'];
        $customerPassword = $params['customer']['telephone'];

        $custom_note = $params['custom_note'];
        
        $customer_group_id = $params['customer_group_id'];

        $customer = Mage::getModel("customer/customer"); 
        $customer->setWebsiteId(Mage::app()->getWebsite()->getId()); 

        if($newcustomerflag == 1){

            $customerFirstName = $params['customer']['firstname'];
            $customerLastName = $params['customer']['lastname'];

            $customer->setEmail($customeremail);
            $customer->setFirstname($customerFirstName);
            $customer->setLastname($customerLastName);
            $customer->setPassword($customerPassword);

            try{
                $customer->save();
                $customer = $customer->loadByEmail($customeremail);
                //Session
                // $session = Mage::getSingleton('customer/session');
                // $session->start();
                // $session->setCustomerAsLoggedIn($customer);
                // $session->login($customerEmail, $customerPassword);
                
                // $this->_createNewAddress($customer);
              }
            catch (Exception $e) {
                //Zend_Debug::dump($e->getMessage());
            } 
        }
        else {
            $customer = $customer->loadByEmail($customeremail);
        }
        
            $firstName = $customer->getFirstname();
            $lastName = $customer->getLastname();
            $region = $params['shipping_address']['region'];
            $regionid = $params['shipping_address']['region_id'];
            $code = $params['shipping_address']['postcode'];
            $city = $params['shipping_address']['city'];
            $street = $params['shipping_address']['street'];
            $location = $params['shipping_address']['location'];
            $phone = $customerPassword;
            // $vat = $params['shipping_address']['vat_id'];

        $shippingAddress = array(
            'firstname' => $firstName,
            'middlename' => '',
            'lastname' => $lastName,
            'street' => array(
                '0' => $street, // compulsory
                '1' => $location // optional
            ),
            'city' => $city,
            'country_id' => 'ID', // two letters country code
            'region' => $region, // can be empty '' if no region
            'region_id' => $regionid, // can be empty '' if no region_id
            'postcode' => $code,
            'telephone' => $phone,
            'fax' => '',
            // 'vat_id' => $vat,
            'save_in_address_book' => 1
        );

        $address = Mage::getModel('customer/address');  
        $address->setData($shippingAddress)
            ->setCustomerId($customer->getId())
            ->setIsDefaultBilling('1')
            ->setIsDefaultShipping('1')
            ->setSaveInAddressBook('1');
        $address->save();

        if($customer_group_id ){
            $customer->setData('group_id', $customer_group_id);
        }
        
        $customer->save();

        // Initialize sales quote object
        $quote = Mage::getModel('sales/quote')->setStoreId($store->getId());

        // Set currency for the quote
        $quote->setCurrency(Mage::app()->getStore()->getBaseCurrencyCode());
                
        // Assign customer to quote
        $quote->assignCustomer($customer);

        foreach($params['quote'] as $productquote){
            $product = Mage::getModel('catalog/product')->load($productquote['product_id']);
            // $quote->addProduct($product, $productquote['request_quantity']);

            $quoteItem = Mage::getModel('sales/quote_item')->setProduct($product);
            $quoteItem->setQuote($quote);
            $quoteItem->setQty($productquote['request_quantity']);
            $quoteItem->setStoreId($store->getId());
            $quote->addItem($quoteItem);
        }

        $quote->collectTotals()->save();

        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'Manual Order Step 1 success',
            'quote_id' => $quote->getId()
        );
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
    }

    public function manualOrderstep1V2Action(){
        $getraws = $this->getRequest()->getRawBody();
        $params = json_decode($getraws, true);

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

        //Create sales quote object
        $quote = Mage::getModel('sales/quote')->setStoreId($store->getStoreId());
            
        $newcustomerflag = $params['newcustomerflag'];
        // $productid = $params['product_id'];
        // $productQty = $params['request_quantity'];
        $customeremail = $params['customer']['email'];
        $customerPassword = $params['customer']['telephone'];

        $custom_note = $params['custom_note'];
        $customer_group_id = $params['customer_group_id'];

        $customer = Mage::getModel("customer/customer"); 
        $customer->setWebsiteId(Mage::app()->getWebsite()->getId()); 

        if($newcustomerflag == 1){

            $customerFirstName = $params['customer']['firstname'];
            $customerLastName = $params['customer']['lastname'];

            $customer->setEmail($customeremail);
            $customer->setFirstname($customerFirstName);
            $customer->setLastname($customerLastName);
            $customer->setPassword($customerPassword);

            try{
                $customer->save();
                $customer = $customer->loadByEmail($customeremail);
                //Session
                // $session = Mage::getSingleton('customer/session');
                // $session->start();
                // $session->setCustomerAsLoggedIn($customer);
                // $session->login($customerEmail, $customerPassword);
                
                // $this->_createNewAddress($customer);
              }
            catch (Exception $e) {
                //Zend_Debug::dump($e->getMessage());
            } 
        }
        else {
            $customer = $customer->loadByEmail($customeremail);
        }
        
            $firstName = $customer->getFirstname();
            $lastName = $customer->getLastname();
            $region = $params['shipping_address']['region'];
            $regionid = $params['shipping_address']['region_id'];
            $code = $params['shipping_address']['postcode'];
            $city = $params['shipping_address']['city'];
            $street = $params['shipping_address']['street'];
            $location = $params['shipping_address']['location'];
            $phone = $customerPassword;
            // $vat = $params['shipping_address']['vat_id'];

        $shippingAddress = array(
            'firstname' => $firstName,
            'middlename' => '',
            'lastname' => $lastName,
            'street' => array(
                '0' => $street, // compulsory
                '1' => $location // optional
            ),
            'city' => $city,
            'country_id' => 'ID', // two letters country code
            'region' => $region, // can be empty '' if no region
            'region_id' => $regionid, // can be empty '' if no region_id
            'postcode' => $code,
            'telephone' => $phone,
            'fax' => '',
            // 'vat_id' => $vat,
            'save_in_address_book' => 1
        );

        $address = Mage::getModel('customer/address');  
        $address->setData($shippingAddress)
            ->setCustomerId($customer->getId())
            ->setIsDefaultBilling('1')
            ->setIsDefaultShipping('1')
            ->setSaveInAddressBook('1');

        
            
        $address->save();

        if($customer_group_id ){
            $customer->setData('group_id', $customer_group_id);
        }
        
        $customer->save();

        // Initialize sales quote object
        $quote = Mage::getModel('sales/quote')->setStoreId($store->getId());

        // Set currency for the quote
        $quote->setCurrency(Mage::app()->getStore()->getBaseCurrencyCode());
                
        // Assign customer to quote
        $quote->assignCustomer($customer);

        foreach($params['quote'] as $productquote){
            $product = Mage::getModel('catalog/product')->load($productquote['product_id']);
            // $quote->addProduct($product, $productquote['request_quantity']);

            $quoteItem = Mage::getModel('sales/quote_item')->setProduct($product);
            $quoteItem->setQuote($quote);
            $quoteItem->setQty($productquote['request_quantity']);
            $quoteItem->setData('margin',$productquote['margin']);
            $quoteItem->setData('transaction_fee',$productquote['transaction_fee']);
           

            if($productquote['custom_price']){
                $quoteItem->setOriginalCustomPrice($productquote['custom_price']);
                $quoteItem->setCustomPrice($productquote['custom_price']);
                $quoteItem->setIsSuperMode(true);
            }

            

           // $quoteItem->setTransactionFee($productquote['transaction_fee']);
            $quoteItem->setStoreId($store->getId());
            $quote->addItem($quoteItem);
        }

   

        

        $quote->collectTotals()->save();
        $quote->save();

        foreach ($quote->getAllItems() as $item){
            $items[] = array(
                'product_id'=>$item->getId(),
                'margin'=>$item->getMargin(),
                'transaction_fee'=>$item->getTransactionFee(),
                
            );
        }
        

        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'Manual Order Step 1 success',
            'quote'=>$items,
            'quote_id' => $quote->getId()
        );
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
    }

    public function manualOrderstep2Action(){

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

        $getraws = $this->getRequest()->getRawBody();
        $params = json_decode($getraws, true);

        $quoteid = $params['quote_id'];
        $quote = Mage::getModel('sales/quote')->load($quoteid);

        $customeremail = $params['customer']['email'];

        $customer = Mage::getModel("customer/customer"); 
        $customer->setWebsiteId(Mage::app()->getWebsite()->getId()); 
        $customer = $customer->loadByEmail($customeremail);

        // $quote = Mage::getModel('sales/quote')->loadByCustomer($customer->getId());

        $firstName = $customer->getFirstname();
            $lastName = $customer->getLastname();
            $region = $params['shipping_address']['region'];
            $regionid = $params['shipping_address']['region_id'];
            $code = $params['shipping_address']['postcode'];
            $city = $params['shipping_address']['city'];
            $street = $params['shipping_address']['street'];
            $location = $params['shipping_address']['location'];
            $phone = $params['customer']['telephone'];
            // $vat = $params['shipping_address']['vat_id'];

        $shippingAddress = array(
            'firstname' => $firstName,
            'middlename' => '',
            'lastname' => $lastName,
            'street' => array(
                '0' => $street, // compulsory
                '1' => $location // optional
            ),
            'city' => $city,
            'country_id' => 'ID', // two letters country code
            'region' => $region, // can be empty '' if no region
            'region_id' => $regionid, // can be empty '' if no region_id
            'postcode' => $code,
            'telephone' => $phone,
            'fax' => '',
            // 'vat_id' => $vat,
            'save_in_address_book' => 1
        );

        // $paymentMethod = 'snapio';
        $paymentMethod = 'banktransfer';

        // $shippingmethod = 'wearinasiapickuppoint_mycarrier_Pickup Store';
        $shippingmethod = $params['selected_shipping_code'];
        // $shippingmethod = 'freeshipping_freeshipping';

        // Add billing address to quote
        $billingAddressData = $quote->getBillingAddress()->addData($shippingAddress);
                    
        // Add shipping address to quote
        $shippingAddressData = $quote->getShippingAddress()->addData($shippingAddress);
        
        // Collect shipping rates on quote shipping address data
        $shippingAddressData->setCollectShippingRates(true)
                            ->collectShippingRates();

        // Set shipping and payment method on quote shipping address data
        $shippingAddressData->setShippingMethod($shippingmethod)->setPaymentMethod($paymentMethod);

        // Set payment method for the quote
        $quote->getPayment()->importData(array('method' => $paymentMethod));

        try {
            // Collect totals of the quote
            $quote->collectTotals();

            // Save quote
            $quote->save();

            // // Create Order From Quote
            $service = Mage::getModel('sales/service_quote', $quote);
            $service->submitAll();
            $incrementId = $service->getOrder()->getRealOrderId();
            $orderId = $service->getOrder()->getId();
            // $this->_sendAPI($quote);

            Mage::getSingleton('checkout/session')
                ->setRealOrderId($incrementId)
                ->setLastRealOrderId($incrementId);
                
            //set comment
            $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
            $order->addStatusHistoryComment($custom_note);
            $order->addStatusHistoryComment('ORDER MANUAL DARI BI');
            $order->save();
            $this->_clearsession();

            $result['success'] = true;
            $result['error'] = false;

            $result = Mage::helper('Wia_Module_helper')->Payment($incrementId);
            // $result = Mage::helper('Wia_Module_helper')->TestPayment($incrementId);

            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Order has been created',
                'order_id' => $order->getId(),
                'payment' => $result,
                'gift_message' => $gift
            );
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
            $this->_clearsession();
        } 
        catch (Mage_Core_Exception $e) {
            //$redirecturl = Mage::getBaseUrl().'snapbarugopay/payment/redirect';
            $result['success'] = false;
            $result['error'] = true;
            $result['error_messages'] = $e->getMessage();
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Order cannot be create, '.$e->getMessage()
            );
            Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
            
            if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                Mage::getSingleton('checkout/session')->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    Mage::getSingleton('checkout/session')->addError(Mage::helper('core')->escapeHtml($message));
                }
            }
        } 
        catch (Exception $e) {
            $result['success']  = false;
            $result['error']    = true;
            $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
            Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                
            Mage::logException($e);
            //$this->_goBack();
        } 
    }


    public function manualOrderstep1V3Action(){
        $getraws = $this->getRequest()->getRawBody();
        $params = json_decode($getraws, true);

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

        //Create sales quote object
        $quote = Mage::getModel('sales/quote')->setStoreId($store->getStoreId());
            
        $newcustomerflag = $params['newcustomerflag'];
        // $productid = $params['product_id'];
        // $productQty = $params['request_quantity'];
        $customeremail = $params['customer']['email'];
        $customerPassword = $params['customer']['telephone'];

        $custom_note = $params['custom_note'];
        $customer_group_id = $params['customer_group_id'];

        $customer = Mage::getModel("customer/customer"); 
        $customer->setWebsiteId(Mage::app()->getWebsite()->getId()); 

        if($newcustomerflag == 1){

            $customerFirstName = $params['customer']['firstname'];
            $customerLastName = $params['customer']['lastname'];

            $customer->setEmail($customeremail);
            $customer->setFirstname($customerFirstName);
            $customer->setLastname($customerLastName);
            $customer->setPassword($customerPassword);

            try{
                $customer->save();
                $customer = $customer->loadByEmail($customeremail);
                //Session
                // $session = Mage::getSingleton('customer/session');
                // $session->start();
                // $session->setCustomerAsLoggedIn($customer);
                // $session->login($customerEmail, $customerPassword);
                
                // $this->_createNewAddress($customer);
              }
            catch (Exception $e) {
                //Zend_Debug::dump($e->getMessage());
            } 
        }
        else {
            $customer = $customer->loadByEmail($customeremail);
        }
        
            $firstName = $customer->getFirstname();
            $lastName = $customer->getLastname();
            $region = $params['shipping_address']['region'];
            $regionid = $params['shipping_address']['region_id'];
            $code = $params['shipping_address']['postcode'];
            $city = $params['shipping_address']['city'];
            $street = $params['shipping_address']['street'];
            $location = $params['shipping_address']['location'];
            $phone = $params['shipping_address']['telephone'];
            // $vat = $params['shipping_address']['vat_id'];

            $shippingAddress = array(
                'firstname' => $firstName,
                'middlename' => '',
                'lastname' => $lastName,
                'street' => array(
                    '0' => $street, // compulsory
                    '1' => $location // optional
                ),
                'city' => $city,
                'country_id' => 'ID', // two letters country code
                'region' => $region, // can be empty '' if no region
                'region_id' => $regionid, // can be empty '' if no region_id
                'postcode' => $code,
                'telephone' => $phone,
                'fax' => '',
                // 'vat_id' => $vat,
                //'save_in_address_book' => 1
            );

            $address = Mage::getModel('customer/address');  
            $address->setData($shippingAddress)
                ->setCustomerId($customer->getId())
                ->setIsDefaultBilling('1')
                ->setIsDefaultShipping('1')
                ->setSaveInAddressBook('1');

            
                
            $address->save();

            if($customer_group_id ){
                $customer->setData('group_id', $customer_group_id);
            }
            
            $customer->save();

        // Initialize sales quote object
        $quote = Mage::getModel('sales/quote')->setStoreId($store->getId());

        // Set currency for the quote
        $quote->setCurrency(Mage::app()->getStore()->getBaseCurrencyCode());
                
        // Assign customer to quote
        $quote->assignCustomer($customer);

        foreach($params['quote'] as $productquote){
            $product = Mage::getModel('catalog/product')->load($productquote['product_id']);
            // $quote->addProduct($product, $productquote['request_quantity']);

            $quoteItem = Mage::getModel('sales/quote_item')->setProduct($product);
            $quoteItem->setQuote($quote);
            $quoteItem->setQty($productquote['request_quantity']);
            $quoteItem->setData('margin',$productquote['margin']);
            $quoteItem->setData('additional_data',$productquote['bonus_value']);
            $quoteItem->setData('transaction_fee',$productquote['transaction_fee']);
           

            if($productquote['custom_price']){
                $quoteItem->setOriginalCustomPrice($productquote['custom_price']);
                $quoteItem->setCustomPrice($productquote['custom_price']);
                $quoteItem->setIsSuperMode(true);
            }

            

           // $quoteItem->setTransactionFee($productquote['transaction_fee']);
            $quoteItem->setStoreId($store->getId());
            $quote->addItem($quoteItem);
        }


       

        $quote->collectTotals()->save();

        $quote->save();

        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'Manual Order Step 1 success',
            'quote_id' => $quote->getId()
        );
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
    }

    public function manualOrderstep2V3Action(){

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

        $getraws = $this->getRequest()->getRawBody();
        $params = json_decode($getraws, true);

        $quoteid = $params['quote_id'];
        $quote = Mage::getModel('sales/quote')->load($quoteid);

        $customeremail = $params['customer']['email'];

        $customer = Mage::getModel("customer/customer"); 
        $customer->setWebsiteId(Mage::app()->getWebsite()->getId()); 
        $customer = $customer->loadByEmail($customeremail);

        // $quote = Mage::getModel('sales/quote')->loadByCustomer($customer->getId());

        $firstName = $customer->getFirstname();
            $lastName = $customer->getLastname();
            $region = $params['shipping_address']['region'];
            $regionid = $params['shipping_address']['region_id'];
            $code = $params['shipping_address']['postcode'];
            $city = $params['shipping_address']['city'];
            $street = $params['shipping_address']['street'];
            $location = $params['shipping_address']['location'];
            $phone = $params['customer']['telephone'];
            // $vat = $params['shipping_address']['vat_id'];

        $shippingAddress = array(
            'firstname' => $firstName,
            'middlename' => '',
            'lastname' => $lastName,
            'street' => array(
                '0' => $street, // compulsory
                '1' => $location // optional
            ),
            'city' => $city,
            'country_id' => 'ID', // two letters country code
            'region' => $region, // can be empty '' if no region
            'region_id' => $regionid, // can be empty '' if no region_id
            'postcode' => $code,
            'telephone' => $phone,
            'fax' => '',
            // 'vat_id' => $vat,
           // 'save_in_address_book' => 1
        );

        // $paymentMethod = 'snapio';
        $paymentMethod = 'banktransfer';

        // $shippingmethod = 'wearinasiapickuppoint_mycarrier_Pickup Store';
        $shippingmethod = $params['selected_shipping_code'];
        // $shippingmethod = 'freeshipping_freeshipping';

        // Add billing address to quote
        $billingAddressData = $quote->getBillingAddress()->addData($shippingAddress);
                    
        // Add shipping address to quote
        $shippingAddressData = $quote->getShippingAddress()->addData($shippingAddress);
        
        // Collect shipping rates on quote shipping address data
        $shippingAddressData->setCollectShippingRates(true)
                            ->collectShippingRates();

        // Set shipping and payment method on quote shipping address data
        $shippingAddressData->setShippingMethod($shippingmethod)->setPaymentMethod($paymentMethod);

        // Set payment method for the quote
        $quote->getPayment()->importData(array('method' => $paymentMethod));

        try {
            // Collect totals of the quote
            $quote->collectTotals();

            // Save quote
            $quote->save();

            // // Create Order From Quote
            $service = Mage::getModel('sales/service_quote', $quote);
            $service->submitAll();
            $incrementId = $service->getOrder()->getRealOrderId();
            $orderId = $service->getOrder()->getId();
           
            //set comment
            $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);



            $discount = $params['discount'];
            $discount_description = $params['discount_description'];
            $newTotal = $order->getGrandTotal() - $discount;


            $order->setData('discount_amount', $discount);
            $order->setData('base_discount_amount', $discount);
            $order->setData('discount_description', $discount_description);

            $order->setData('grand_total',$newTotal);
            $order->setData('base_grand_total',$newTotal);

            
            $order->addStatusHistoryComment($custom_note);
            $order->addStatusHistoryComment('ORDER MANUAL DARI BI');


            $order->save();
            
            $this->_clearsession();

            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Order has been created',
                'order_id' => $order->getId(),
              
                
            );


            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
            $this->_clearsession();
        } 
        catch (Mage_Core_Exception $e) {
            //$redirecturl = Mage::getBaseUrl().'snapbarugopay/payment/redirect';
            $result['success'] = false;
            $result['error'] = true;
            $result['error_messages'] = $e->getMessage();
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Order cannot be create, '.$e->getMessage()
            );
            Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
            
           
        } 
        catch (Exception $e) {
            $result['success']  = false;
            $result['error']    = true;
            $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
            Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                
            Mage::logException($e);
            //$this->_goBack();
        } 
    }
   


    public function ManualShippingListAction(){

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

        // $customer = $this->_getCustomer();
        $getraws = $this->getRequest()->getRawBody();
        $params = json_decode($getraws, true);
        $customeremail = $params['customer']['email'];

        $customer = Mage::getModel("customer/customer"); 
        $customer->setWebsiteId(Mage::app()->getWebsite()->getId()); 
        $customer = $customer->loadByEmail($customeremail);

        $customerShippingAddress = $customer->getPrimaryShippingAddress();
        // Update the cart's quote.
        // $cart = Mage::getSingleton('checkout/cart');
        // $address = $cart->getQuote()->getShippingAddress();

        $address = Mage::getModel("customer/address")->load($customerShippingAddress->getId());
        $address->setIsDefaultBilling('1');
        $address->setIsDefaultShipping('1');
        $address->save();

        $quoteid = $params['quote_id'];
        $quote = Mage::getModel('sales/quote')->load($quoteid);
        //$address = $quote->getShippingAddress();
        $cart = Mage::getSingleton('checkout/cart');
        $address = $cart->getQuote()->getShippingAddress();

        $address->setCountryId($customerShippingAddress->getCountryId())
                ->setPostcode($customerShippingAddress->getPostcode())
                ->setCity($customerShippingAddress->getCity())
                ->setRegionId($customerShippingAddress->getRegionId())
                ->setRegion($customerShippingAddress->getRegion())
                ->setStreet(array(
                    '0' => $customerShippingAddress->getStreet(1), // compulsory
                    '1' => $customerShippingAddress->getStreet(2) // optional
                ))
                ->setTelephone($customerShippingAddress->getTelephone())
                ->setData('vat_id', $customerShippingAddress->getData('vat_id'))
                ->setFirstname($customer->getFirstname())
                ->setLastname($customer->getLastname())->setCollectShippingrates(true);
        $address->save();
        $cart->save();
        
        // Collect shipping rates on quote shipping address data
        $quote->getShippingAddress()->setCollectShippingRates(true)->collectShippingRates();
        //$quote->collectTotals()->save();

        // Find if our shipping has been included.
        $rates = $address->collectShippingRates()
        ->getGroupedAllShippingRates();

        $shipping = $address->collectShippingRates()->getGroupedAllShippingRates();

        if($shipping == null){
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Shipping List Null'
            );
        }
        else {
            foreach($shipping as $methods => $rates){
                foreach ($rates as $rate){
                    if($rate['code'] == 'flatrate_flatrate'){}
                    else {
                        $array[] = array(
                            'carrier' => $rate['carrier'],
                            'carrier_title' => $rate['carrier_title'],
                            'code' => $rate['code'],
                            'method' => $rate['method'],
                            'method_description' => $rate['method_description'],
                            'price' => $rate['price'],
                            'error_message' => $rate['error_message'],
                            'method_title' => $rate['method_title'],
                            'carrierName' => $rate['carrierName']
                        );
                    }
                }
            }
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'GET Shipping List success',
                'id' => $quote->getId(),
                'shipping' => $array
            );
        }
        $this->_sendAPI($message_code); 
    }

    public function getCustomerGroupAction(){
       $group = Mage::helper('customer')->getGroups();

       foreach($group as $customergroup){
           $array[] = array(
               'customer_group_id' => $customergroup->getCustomerGroupId(),
               'customer_group_code' => $customergroup->getCustomerGroupCode()
           );
       }

       $this->_sendAPI($array);
    }

    public function GiftAction(){

        $quote = Mage::helper('checkout/cart')->getCart()->getQuote();
        $customer = $customer = $this->_getCustomer();

        $getraws = $this->getRequest()->getRawBody();
        $params = json_decode($getraws, true);

        $recepient = $params['recepient'];
        $message = $params['message'];

        if(!empty($recepient) && !empty($message)){
            if($quote->getGiftMessageId() != 'null'){
                $giftMessage = Mage::getModel('giftmessage/message')->load($quote->getGiftMessageId()); 
                $giftMessage->setCustomerId($customer->getId()); 
                $giftMessage->setSender($customer->getFirstname().' '.$customer->getLastname()); 
                $giftMessage->setRecipient($recepient); 
                $giftMessage->setMessage($message); 
                $giftObj = $giftMessage->save(); 

                $quote->setGiftMessageId($giftObj->getId()); 
                $quote->save();

                if(!empty($giftMessage)){
                    $message_code = array( 
                        'message_code' => 200,
                        'message_dialog' => 'Gift Message has been edited',
                        'gift_message' => $giftMessage->getData()
                    );
                }
                else {
                    $message_code = array( 
                        'message_code' => 400,
                        'message_dialog' => 'Failed to edit gift message',
                        'gift_message' => $giftMessage->getData()
                    );
                }
            }
            else {
                $giftMessage = Mage::getModel('giftmessage/message'); 
                $giftMessage->setCustomerId($customer->getId()); 
                $giftMessage->setSender($customer->getFirstname().' '.$customer->getLastname()); 
                $giftMessage->setRecipient($recepient); 
                $giftMessage->setMessage($message); 
                $giftObj = $giftMessage->save(); 

                $quote->setGiftMessageId($giftObj->getId()); 
                $quote->save();

                if(!empty($giftMessage)){
                    $message_code = array( 
                        'message_code' => 200,
                        'message_dialog' => 'Gift Message has been created',
                        'gift_message' => $giftMessage->getData()
                    );
                }
                else {
                    $message_code = array( 
                        'message_code' => 400,
                        'message_dialog' => 'Failed to make gift message',
                        'gift_message' => $giftMessage->getData()
                    );
                }
            }
        }
        else {
            $message_code = array( 
                'message_code' => 403,
                'message_dialog' => 'Message and Recepient are required'
            );
        } 
        $this->_sendAPI($message_code);
    }

    public function _gift($data, $data1, $data2){
        $customer = $customer = $this->_getCustomer();
        $order = $data;
        $recepient = $data1;
        $message = $data2;

        if(!empty($recepient) && !empty($message)){
            
                $giftMessage = Mage::getModel('giftmessage/message'); 
                $giftMessage->setCustomerId($customer->getId()); 
                $giftMessage->setSender($customer->getFirstname().' '.$customer->getLastname()); 
                $giftMessage->setRecipient($recepient); 
                $giftMessage->setMessage($message); 
                $giftObj = $giftMessage->save(); 

                $order->setGiftMessageId($giftObj->getId()); 
                $order->save();

                if(!empty($giftMessage)){
                    return $order->getGiftMessageId();
                }
                else {
                    $message_code = array( 
                        'message_code' => 400,
                        'message_dialog' => 'Failed to make gift message',
                        'gift_message' => $giftMessage->getData()
                    );
                }
            
        }
        else {
            $message_code = array( 
                'message_code' => 403,
                'message_dialog' => 'Message and Recepient are required'
            );
        } 
    }

    public function GetGiftAction(){

        $params = $this->getRequest()->getParams();
        $id = $params['id'];
        
        $giftMessage = Mage::getModel('giftmessage/message')->load($id); 

        if(!empty($giftMessage)){
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Get Gift Message Success',
                'gift_message' => $giftMessage->getData()
            );
        }
        else {
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Failed to get gift message',
                'gift_message' => $giftMessage->getData()
            );
        }

        $this->_sendAPI($message_code);
    }
}

?>
