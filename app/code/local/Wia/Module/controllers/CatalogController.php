<?php
    class Wia_Module_CatalogController extends Mage_Core_Controller_Front_Action
    {

        public function indexAction(){
            $product_id = 237; 
            $product = Mage::getModel('catalog/product')->load($product_id);
               if($product->getTypeId() == 'configurable'){
                   $productAttributeOptions = $product->getTypeInstance(true)->getConfigurableAttributesAsArray($product);
               
               

                   $productTypeInstance = $product->getTypeInstance();
                   $usedProducts = $productTypeInstance->getUsedProducts($product);
                       foreach ($usedProducts as $item) {
                           $items [] = array(
                               'value_index' => $item->getSize(),
                               'stock' => $item->getStockItem()->getStockStatus()
                           );
                       }
               }

           



           $response = array( 
               'options' => $productAttributeOptions,  
               'availability'=> $items,
               'related_product'=> $this->_getRelatedProduct($product),
              
           );
           $this->_sendAPI($response);
          
          

           
        }


        public function updateProductAction(){
            $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*') // select all attributes
            ->setPageSize(5000) // limit number of results returned
            ->setCurPage(1);

            $attributes=array(
                'price',
                'special_price',
              

            );

            foreach($collection as $item){
                echo $item->getId();
                foreach($attributes as $attribute){
                    $this->forceProductsToUseDefault($attribute,1,$item->getId());
                }
                
            }

            
        }

        public function forceProductsToUseDefault($attributeCode, $storeCode = null, $productIds = null)
            {
                $conditions = array();

                if (is_null($storeCode)) {
                    $conditions['store_id != ?'] = Mage_Core_Model_App::ADMIN_STORE_ID;
                } else {
                    $store = Mage::app()->getStore($storeCode);
                    if (!$store instanceof Mage_Core_Model_Store || !$store->getId()) {
                        Mage::throwException("Store with code not found: $storeCode");
                    }
                    $conditions['store_id = ?' ] = $store->getId();
                }

                if (!is_null($productIds)) {
                    $conditions['entity_id in(?)'] = $productIds;
                }

                $attribute = Mage::getModel('eav/entity_attribute')
                    ->loadByCode(Mage_Catalog_Model_Product::ENTITY, $attributeCode);
                if (!$attribute instanceof Mage_Eav_Model_Entity_Attribute_Abstract || !$attribute->getId()) {
                    Mage::throwException("Attribute with code not found: $attributeCode");
                }

                $conditions['attribute_id = ?'] = $attribute->getId();

                $coreResource = Mage::getSingleton('core/resource');

                $coreResource->getConnection('core_write')->delete(
                    $coreResource->getTableName(array('catalog/product', $attribute->getData('backend_type'))),
                    $conditions
                );
            }


        private function _updateInventory($order_qty,$product_id){
        
            $request =  $order_qty;
            $stocks = Mage::getModel('Module/inventoryitem')
            ->getCollection()
            ->addFieldToFilter('is_sold', '0')
            ->addFieldToFilter('product_id', $product_id);
            //->addFieldToFilter('is_sold', '0');
     
            if($stocks->getSize() > 0){
                try {
    
                    foreach($stocks as $stock){
                        $stock_left = $stock->getStockLeft();
    
                    if($stock_left-$request < 0){
                            $stock->setStockLeft(0);
                            $stock->setIsSold(1);
                    }
                    else{
                            $stock->setStockLeft($stock_left-$request);
                    }
                    
                    $stock->save();
                    $response[] = $stock->toArray();
    
                    $request = $request - $stock_left;
                    
                    if($request <= 0){
                            break;
                    }
    
                    
                    
                }
                } catch (\Throwable $th) {
                    //throw $th;
                }
    
                return $response;
            }
            
        }
        

        public function _getChildCategory($id){
            $parrent_category = Mage::getModel('catalog/category')->load($id);
            $childCategorys = $parrent_category->getChildrenCategories();
            foreach ( $childCategorys as $category) {
                $response[] = array( 
                    'entity_id' => $category->getId(),
                    'name' => $category->getName(),
                    'url_path' => $category->getRequestPath(),  
                );
            }
            return $response;
        }

        public function _getWebp($file_name){
            $path_parts = pathinfo( $file_name);
            

            $imageUrls = Mage::getBaseDir ( 'media' ) . DS . "catalog" . DS . "category" . DS . $file_name;
            $imageResized = Mage::getBaseDir ( 'media' ) . DS . "catalog" . DS . "category" . DS . "cache" . DS .  $path_parts['filename'] .'.webp';
            if (! file_exists ( $imageResized )) :
                $image = imagecreatefromstring(file_get_contents($imageUrls));
                ob_start();
                imagejpeg($image,NULL,80);
                $cont = ob_get_contents();
                ob_end_clean();
                imagedestroy($image);
                $content = imagecreatefromstring($cont);
                $output = Mage::getBaseDir ( 'media' ) . DS . "catalog" . DS . "category" . DS . "cache" . DS .  $path_parts['filename'].'.webp';
                imagewebp($content,$output,80);
                imagedestroy($content);
                $url = Mage::getBaseUrl ( 'media' ) . "catalog" . DS . "category" . DS . "cache" . DS . $path_parts['filename'].'.webp';
            endif;

            if(file_exists($imageResized)){
    
                $url = Mage::getBaseUrl ( 'media' ) . "catalog" . DS . "category" . DS . "cache" . DS . $path_parts['filename'] .'.webp';
                return $url;
                //return Mage::getBaseUrl ( 'media' ) ."catalog/product/cache/cat_resize/" . $this->getData("sw_ocat_category_icon_image");
            }
           
            
                   
        }
      
 
        public function _sendAPI($data){
            $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));

        }


        public function _getPromoById($id){
            $promo = Mage::getModel('salesrule/rule')->load($id);
            $response = array(
                'code' => $promo->getCouponCode(),
                'description' => $promo->getDescription(),
            );
            return $response ;
        }

        public function _getProductImages($id){
            $product = Mage::getModel('catalog/product')->load($id); 
            foreach ($product->getMediaGalleryImages() as $image) 
            { 
                $response[] = array( 
                    'image' => (string)Mage::helper('catalog/image')->init($product, 'small_image', $image->getFile())->resize(175,125),
                    'image_2x' => (string)Mage::helper('catalog/image')->init($product, 'small_image',  $image->getFile())->resize(700,500),
                    'image_3x' => (string)Mage::helper('catalog/image')->init($product, 'small_image',  $image->getFile())->resize(700,500),
                
                );
            
            }
            return $response;
        }

        public function _getInstallment($data){

            $installments = $data;

            $pieces[] = explode(",", $installments);

            foreach($pieces as $value){
                $arr = $value;
            }
            if ($arr = ["415","158","157","156"]){
                $installment = 24;
            }
            else if ($arr = ["158","157","156"]){
                $installment = 12;
            }
            else if(["158","157"]){
                $installment = 6;
            }
            else if(["158"]){
                $installment = 3;
            }
            else {}
            
            return $installment;
        }


        public function getProductOptionAction(){
            $product_id = $this->getRequest()->getParam('entity_id'); 
             $product = Mage::getModel('catalog/product')->load($product_id);
                if($product->getTypeId() == 'configurable'){
                    $productAttributeOptions = $product->getTypeInstance(true)->getConfigurableAttributesAsArray($product);
                
                

                    $productTypeInstance = $product->getTypeInstance();
                    $usedProducts = $productTypeInstance->getUsedProducts($product);
                        foreach ($usedProducts as $item) {
                            $items [] = $item->getData('is_in_stock');
                            $size [] = array(
                                'value_index' => $item->getSize(),
                                'stock' => $item->getStockItem()->getStockStatus()
                            );
                            
                        }
                }

            



            $response = array( 
                'options' => $productAttributeOptions,  
                'availability'=> $items,
                'size'=> $size,
                'related_product'=> $this->_getRelatedProduct($product),
               
            );
            $this->_sendAPI($response);
        }

        function getChildrenStockQTY($product, $stock_qty = 0) {
    if ($product->getTypeId() != 'configurable') return false;
    $simple_ids = Mage::getResourceSingleton('catalog/product_type_configurable')->getChildrenIds($product->getId());
    foreach ($simple_ids[0] as $simple_id) {
        $simple_model = Mage::getModel('cataloginventory/stock_item')->loadByProduct($simple_id);
        $stock_qty += $simple_model->getQty();
    }
    return $stock_qty;
}


       
      

        public function _getProducts($category_id){
            $page = $this->getRequest()->getParam('p');
            $products = Mage::getModel('catalog/category')->load($category_id)
            ->getProductCollection()
            ->addAttributeToSelect('*') // add all attributes - optional
            ->addAttributeToFilter('status', 1) // enabled
            ->addAttributeToFilter('visibility', 4) //visibility in catalog,search
            ->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
            ->setOrder('inventory_in_stock', 'desc')
            //->setOrder('price', 'asc')
            ->setOrder('created_at', 'desc')
            ->setCurPage($page) // 2nd page
            
            ->setPageSize(12);

            $brand = $this->getRequest()->getParam('brand');
            $size = $this->getRequest()->getParam('size');
            $activity = $this->getRequest()->getParam('activity');
            $brandfilter = '';
            $sizefilter = '';
            $activityfilter = '';
            
            if($brand != null){
                $keywords = explode(",", $brand);
                foreach($keywords as $value){
			        $brandfilter[] = array('attribute' => 'brand', 'like' => '%'.$value.'%');
                }
            }
            if($size != null){
                $keywords = explode(",", $size);
                foreach($keywords as $valuesize){
			        $sizefilter[] = array('attribute' => 'size', 'like' => '%'.$valuesize.'%');
                }
            }

            if($activity != null){
                $keywords = explode(",", $activity);
                foreach($keywords as $valueactivity){
			        $activityfilter[] = array('attribute' => 'activity', 'like' => '%'.$valueactivity.'%');
                }
            }
                
            $products->addAttributeToFilter($brandfilter);
            $products->addAttributeToFilter($sizefilter);
            $products->addAttributeToFilter($activityfilter);

            $count = $products->getSize();

            foreach ($products as $product){
                // if($product->getStatus() != 1 ){
                //     continue;
                // }
                
                $attributes[] = $product->getFilterableAttributes();
                if(!empty($product->getPromoId())){
                    $promo = $this->_getPromoById($product->getPromoId());
                }
                else {
                    $promo = 0;
                }

                $items[] = array( 
                    'entity_id' => $product->getId(),
                    'url' => $product->getProductUrl(),
                    'url_path' => $product->getUrlPath(),
                    'url_key' => $product->getUrlKey(),
                    'brand' => $product->getAttributeText("brand"),
                    'brand_id' => $product->getBrand(),
                    'name' => $product->getName(),
                    'price' => $product->getPrice(),
                    'final_price' => $product->getFinalPrice(),
                    'special_price' => $product->getSpecialPrice(),
                    'promo' => $promo,
                    'bundle_product' =>  $product->getFreeProductId()?$this->_getBundleProductById($product->getFreeProductId()):null,
                    'is_in_stock' => $product->getStockItem()->getIsInStock(),
                    'featured_image' => array(
                        'image' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(50,50),
                        'image_2x' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(200,200),
                        'image_3x' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(400,400),
                        // 'image_4x' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(800,600),
                    ),
                    // 'description' => $product->getDescription(),
                    'brand_url' => $product->getBrandUrl(),
                    'installment' => $this->_getInstallment($product->getInstallment()),
                    'is_promo' => $product->getNonPromoItem(),
                    'sku' => $product->getSku(),
                    'margin' => $product->getCost()
                    // 'short_description' => $product->getShortDescription(),
                    // 'description' => $product->getDescription(),
                );
            }
           
            $response = array(
                'count' =>$count,
                'page'  => $page,
                'items' => $items
            );
           return $response;
        }

        public function _getPathId($type){
            $path = $this->getRequest()->getParam('id'); 
            $store = Mage::app()->getStore();
            $rewrite = Mage::getModel('core/url_rewrite')
            ->setStoreId($store->getId())
            ->loadByRequestPath($path);

            if($type == 'category_id'){
                return $rewrite->getCategoryId();
            }
            if($type == 'product_id'){
                return $rewrite->getProductId();
            }
           
            
            
        }

        public function _getBundledProduct($product_id){
            $bundledproduct = Mage::getModel('catalog/product')->load($product_id);
            if($bundledproduct->getTypeId() == 'configurable'){
                $productAttributeOptions = $bundledproduct->getTypeInstance(true)->getConfigurableAttributesAsArray($bundledproduct);
            }

            if($bundledproduct->getNonPromoItem() == 1){
                $is_promo = 0;
            } else {
                $is_promo = 1;
            }
            if($bundledproduct->getStatus() == 1 ){
                $array = array( 
                    'entity_id' => $bundledproduct->getId(),
                    'name' => mb_strimwidth($bundledproduct->getName(), 0, 40, "..."),
                    'images' => $this->_getProductImages($bundledproduct->getId()),
                );
            }
            return $array;
        }
      
        public function getFilterByCategoryAction(){
            
            $id = $this->getRequest()->getParam('id'); 
            $layer = Mage::getModel("catalog/layer");
            $category = Mage::getModel("catalog/category")->load($id);
            $layer->setCurrentCategory($category);
            $attributes = $layer->getFilterableAttributes();
            $count = 0;
            foreach ($attributes as $attribute) {
                    if ($attribute->getAttributeCode() == 'price') {
                        $filterBlockName = 'catalog/layer_filter_price';
                    } elseif ($attribute->getBackendType() == 'decimal') {
                        $filterBlockName = 'catalog/layer_filter_decimal';
                    } else {
                        $filterBlockName = 'catalog/layer_filter_attribute';
                    }

                    $result = Mage::app()->getLayout()->createBlock($filterBlockName)->setLayer($layer)->setAttributeModel($attribute)->init();
                  
                    $attributecode = $attribute->getAttributeCode();
                   
                    foreach($result->getItems() as $option) {
                        
                        $array[$count][] = array(
                            'label' => $option->getLabel(),
                            'value' => $option->getValue()
                        );
                        }
                        if($array[$count] == !null) {
                            $print[$count] = array(
                                'code' => $attributecode,
                                'options' => $array[$count]
                            );
                            $count = $count + 1;
                        }
                       
            }
            $response = Array(
                'child_category' => $this->_getChildCategory($id),
                'filter' => $print
               ); 

            $this->_sendAPI( $response);
        }

        public function _getFilterOption($attribute){
            $layer = Mage::getModel("catalog/layer");      
            $filterBlockName = 'catalog/layer_filter_attribute';
            $result = Mage::app()->getLayout()->createBlock($filterBlockName)->setLayer($layer)->setAttributeModel($attribute)->init();
            foreach($result->getItems() as $option) {
               $response = Array(
                'label' => $option->getLabel()
             
               ); 
            }
            return $response; 
        }

        public function searchAction(){
            $search = $this->getRequest()->getParam('q'); 
            $page = $this->getRequest()->getParam('p'); 
            $keywords = explode(" ", $search);
           

            if($search !==null){
                $products = Mage::getModel("catalog/product")
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('visibility', 4)
                ->addAttributeToFilter('status', 1)
                ->setOrder('created_at', 'desc')
                ->setOrder('inventory_in_stock', 'desc')
                ->joinField('is_in_stock',
                'cataloginventory/stock_item',
                'is_in_stock',
                'product_id=entity_id',
                'is_in_stock=1',
                '{{table}}.stock_id=1',
                'left')
              
               ->setPageSize(100);
                

                foreach ($keywords as $keyword => $value){
                    $products->addAttributeToFilter('name', array('like' => '%'.$value.'%'));
                }
               //$products->addAttributeToFilter('name', array('fjallraven' => '1')); 

               $products->setFlag('require_stock_items', true);

                

              //  $products->load();
                $count = $products->getSize();
                foreach ($products as $product){
                    // if($product->getStatus() != 1 ){
                    //     continue;
                    // }
                   
                    $items[] = array( 
                     
                        'entity_id' => $product->getId(),
                        'margin' => $product->getCost(),
                        // 'url' => $product->getProductUrl(),
                        'url_path' => $product->getUrlPath(),
                        'url_key' => $product->getUrlKey(),
                        'brand' => $product->getAttributeText("brand"),
                        // 'brand_id' => $product->getBrand(),
                        'name' => mb_strimwidth($product->getName(), 0, 40, "..."),
                        'price' => $product->getPrice(),
                        'final_price' => $product->getFinalPrice(),
                        'special_price' => $product->getSpecialPrice(),
                        'promo' => $this->_getPromoById($product->getPromoId()),
                        'is_in_stock' => $product->getStockItem()->getIsInStock(),
                        'featured_image' => array(
                            'image' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(200,250),
                            'image_2x' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(400,500),
                        
                            'image_3x' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(400,500),
                            // 'image_4x' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(800,600),
                        ),
                        'qty'           => $product->getStockItem()->getQty(),
                        
                        // 'description' => $product->getDescription(),
                        // 'brand_url' => $product->getBrandUrl(),
                        'installment' => $this->_getInstallment($product->getInstallment()),
                        'bundle_product' =>  $product->getFreeProductId()?$this->_getBundleProductById($product->getFreeProductId()):null,
                 
                        // 'short_description' => $product->getShortDescription(),
                        // 'description' => $product->getDescription(),
              
                    
                    );
                }
                $response = array(
                    'count' =>$count,
                    'page'  => $page,
                    'items' => $items,
                );
            }
                           
            
            $this->_sendAPI($response);
           
        }

        
    public function searchV2Action(){
        $search = $this->getRequest()->getParam('q'); 
        $page = $this->getRequest()->getParam('p'); 
        $keywords = explode(" ", $search);
       

        if($search !==null){
            $products = Mage::getModel("catalog/product")
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status', 1)
            ->setOrder('created_at', 'desc')
            ->setPageSize(20);
                
           

            foreach ($keywords as $keyword => $value){
                $products->addAttributeToFilter('name', array('like' => '%'.$value.'%'));
            }
           //$products->addAttributeToFilter('name', array('fjallraven' => '1')); 

           $products->setFlag('require_stock_items', true);

            

          //  $products->load();
            $count = $products->getSize();
            foreach ($products as $product){
                // if($product->getStatus() != 1 ){
                //     continue;
                // }
               
                $items[] = array( 
                 
                    'entity_id' => $product->getId(),
                    'margin' => $product->getCost(),
                    // 'url' => $product->getProductUrl(),
                    'url_path' => $product->getUrlPath(),
                    'url_key' => $product->getUrlKey(),
                    'brand' => $product->getAttributeText("brand"),
                    // 'brand_id' => $product->getBrand(),
                    'name' =>  $product->getName(),
                    'price' => $product->getPrice(),
                    'final_price' => $product->getFinalPrice(),
                    'special_price' => $product->getSpecialPrice(),
                    'featured_image' => array(
                        'image' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(200,250),
                              // 'image_4x' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(800,600),
                    ),
                    'qty'           => $product->getStockItem()->getQty(),
                    
                    'stock_buffer'           => $product->getStockBuffer(),
                    
             
                
                );
            }
            $response = array(
                'count' =>$count,
                'page'  => $page,
                'items' => $items,
            );
        }
                       
        
        $this->_sendAPI($response);
       
    }

        // CAMPAIGN

        public function getCampaignsAction(){
           
            $parrent_category = Mage::getModel('catalog/category')->load(756);
            $childCategorys = $parrent_category->getChildrenCategories();

            foreach($childCategorys as $childCategory){
                $category = Mage::getModel('catalog/category')->load($childCategory->getId());
                $response[] = array( 
                    'entity_id' => $category->getId(),
                    'name' => $category->getName(),
                    'label' => $category->getData('category_label'),
                    'title' => $category->getData('category_title'),
                    'description' => $category->getDescription(),
                    'short_description' => $category->getCategoryShortDescription(),
                    'url' => $category->getUrl(),
                    'url_key' => $category->getUrlKey(),
                    'url_path' => $category->getUrlPath(),
                    'promo' => $category->getData('category_promo'),
                    'featured_image' => array(
                        'image' => $category->getResizedThumbnailImage(200,250,78),
                        'image_2x' => $category->getResizedThumbnailImage(400,500,78),
                        'image_3x' => $category->getResizedThumbnailImage(800,1000,78),
                    ),
                    'profile_image' => array(
                        'image' => $category->getResizedImageHelper( $category->getData('menu_menu_icon'),200,200,78),
                        'image_2x' => $category->getResizedImageHelper( $category->getData('menu_menu_icon'), 400,400,78),
                        
                    ),
                   
                    );
            }
            if($response == null){
                $response = [];
            }
            
             $this->_sendAPI($response);
            
        }

        public function getCampaignViewAction(){
            $path = substr($this->getRequest()->getParam('id'),1); 
            $store = Mage::app()->getStore();
            $rewrite = Mage::getModel('core/url_rewrite')
            ->setStoreId($store->getId())
            ->loadByRequestPath($path);
            
            $category_id = $rewrite->getCategoryId();
            $category = Mage::getModel('catalog/category')->load($category_id);
            $price = $category->getData('campaign_sales');


            $fromdate = Mage::getModel('core/date')->date('Y-m-d H:i:s', strtotime($category->getData('promo_start_date')));
            $todate = Mage::getModel('core/date')->date('Y-m-d H:i:s', strtotime($category->getData('promo_end_date')));

            $array = Mage::helper('Wia_Module_helper')->Sales($category_id, $fromdate, $todate, $price);
            $print = array(
                'product_count' => $array['product_count'],
                'campaign_participant' => $array['total_transactions'],
                'campaign_progress' => $array['campaign'],
                'campaign_donation' => $array['campaign'],
            );

            $response = array( 
                'entity_id' => $category->getId(),
                'name' => $category->getName(),
                
                'label' => $category->getData('category_label'),
                'title' => $category->getData('category_title'),
                'description' => Mage::helper("cms")->getBlockTemplateProcessor()->filter($category->getDescription()),
                'short_description' => $category->getCategoryShortDescription(),
                'url' => $category->getUrl(),
                'url_key' => $category->getUrlKey(),
                'url_path' => $category->getUrlPath(),
                'promo' => $category->getData('category_promo'),
                'promo_end_date' => Mage::getModel('core/date')->date('d.m.Y', strtotime($category->getData('promo_end_date'))),
                'term_conditions' =>  $category->getData('show-term'),
                'campaign_progress' =>  $array['campaign'],
                'campaign_participant' =>  $array['total_transactions'],
                'featured_image' => array(
                    'image' => $category->getResizedThumbnailImage(200,250,78),
                    'image_2x' => $category->getResizedThumbnailImage(400,500,78),
                    'image_3x' => $category->getResizedThumbnailImage(800,1000,78),
                ),
                'profile_image' => array(
                    'image' => $category->getResizedImageHelper( $category->getData('menu_menu_icon'),200,200,78),
                    'image_2x' => $category->getResizedImageHelper( $category->getData('menu_menu_icon'), 400,400,78),
                    
                )
            
            );
            
            $this->_sendAPI($response);
            
        }

        // SALES
        public function getSalesAction(){
           
            $parrent_category = Mage::getModel('catalog/category')->load(51);
            $childCategorys = $parrent_category->getChildrenCategories();

            foreach($childCategorys as $childCategory){
                $category = Mage::getModel('catalog/category')->load($childCategory->getId());
                $response[] = array( 
                    'entity_id' => $category->getId(),
                
                    'name' => $category->getName(),
                    'label' => $category->getData('category_label'),
                    'title' => $category->getData('category_title'),
                    'description' => $category->getDescription(),
                    'short_description' => $category->getCategoryShortDescription(),
                    'url' => $category->getUrl(),
                    'url_key' => $category->getUrlKey(),
                    'url_path' => $category->getUrlPath(),
                    'promo' => $category->getData('category_promo'),
                    'featured_image' => array(
                        'image' => $category->getResizedThumbnailImage(200,250,78),
                        'image_2x' => $category->getResizedThumbnailImage(400,500,78),
                        'image_3x' => $category->getResizedThumbnailImage(800,1000,78),
                        'image_4x' => $category->getResizedThumbnailImage(1600,2000,78),
                    ),
                   
                    );
            }
            
             $this->_sendAPI($response);
            
        }

        public function getSaleViewAction(){

            $category_id = $this->_getPathId('category_id');
            $category = Mage::getModel('catalog/category')->load($category_id);

            $response = array( 
                'entity_id' => $category->getId(),
                //'child_category' => $this->_getChildCategory($category->getId()),
                'name' => $category->getName(),
                'label' => $category->getData('category_label'),
                'title' => $category->getData('category_title'),
                'description' => $category->getDescription(),
                'short_description' => $category->getCategoryShortDescription(),
                'url' => $category->getUrl(),
                'url_key' => $category->getUrlKey(),
                'url_path' => $category->getUrlPath(),
                'promo' => $category->getData('category_promo'),
                'featured_image' => array(
                    'image' => $category->getResizedThumbnailImage(200,250,78),
                    'image_2x' => $category->getResizedThumbnailImage(400,500,78),
                    'image_3x' => $category->getResizedThumbnailImage(800,1000,78),
                    'image_4x' => $category->getResizedThumbnailImage(1600,2000,78),
                ),
                'products' => $this->_getProducts($category_id),
               
            );
            
            $this->_sendAPI($response);
            
        }
        
        public function getCampaignSalesAction(){
            $params = $this->getRequest()->getParams();
            $fromdate = $params['from'];
            $todate = $params['to'];
            $brand = $params['brand_id'];

            $array = Mage::helper('Wia_Module_helper')->Sales($brand, $fromdate, $todate);
            $print = array(
                'product_count' => $array['product_count'],
                'campaign_participant' => $array['total_transactions'],
                'campaign_progress' => $array['campaign'],
                'campaign_donation' => $array['campaign'],
            );
            $this->_sendAPI($print);
        }

        //PROMO
        public function getPromoAction(){

            $params = $this->getRequest()->getParams();
            if($params['type']=='sale'){
                $id=787;
            }
            if($params['type']=='partnership'){
                $id=373;
            }
            if($params['type']=='internal'){
                $id=872;
            }
            
            $parrent_category = Mage::getModel('catalog/category')->load($id);
            $childCategorys = $parrent_category->getChildrenCategories();


            foreach($childCategorys as $childCategory){
                
                $category = Mage::getModel('catalog/category')->load($childCategory->getId());
                $response[] = array( 
                    'entity_id' => $category->getId(),
                    'code'=>null,
                    'name' => $category->getName(),
                    'label' => $category->getData('category_label'),
                    'title' => $category->getData('category_title'),
                    'description' => $category->getCategoryShortDescription(),
                    'short_description' => $category->getCategoryShortDescription(),
                    'url' => $category->getUrl(),
                    'url_key' => $category->getUrlKey(),
                    'url_path' => $category->getUrlPath(),
                    'from_date'=> null,
                    'to_date'=> null,
                    'discount_amount'=> null,
                    'image' => $category->getResizedThumbnailImage(400,500,78),
                   
                   
                    );
            }
            
             $this->_sendAPI($response);
            
        }

        public function getPromoViewAction(){

            $category_id = $this->_getPathId('category_id');
            $category = Mage::getModel('catalog/category')->load($category_id);

            $response = array( 
                'entity_id' => $category->getId(),
                'code'=>null,
                'from_date'=> null,
                'to_date'=> null,
                //'child_category' => $this->_getChildCategory($category->getId()),
                'name' => $category->getName(),
                'label' => $category->getData('category_label'),
                'title' => $category->getData('category_title'),
                'description' => $category->getDescription(),
                'term_conditions' =>  $category->getData('show-term'),
                'short_description' => $category->getCategoryShortDescription(),
                'url' => $category->getUrl(),
                'url_key' => $category->getUrlKey(),
                'url_path' => $category->getUrlPath(),
                'promo' => $category->getData('category_promo'),
                'featured_image' => array(
                    'image' => $category->getResizedThumbnailImage(200,250,78),
                    'image_2x' => $category->getResizedThumbnailImage(400,500,78),
                    'image_3x' => $category->getResizedThumbnailImage(800,1000,78),
                    'image_4x' => $category->getResizedThumbnailImage(1600,2000,78),
                ),
                'products' => $this->_getProducts($category_id),
               
            );
            
            $this->_sendAPI($response);
            
        }
        

        // BRAND
        public function getBrandsAction(){
           
            $parrent_category = Mage::getModel('catalog/category')->load(19);
            $childCategorys = $parrent_category->getChildrenCategories();

            foreach($childCategorys as $childCategory){
                $category = Mage::getModel('catalog/category')->load($childCategory->getId());
                $response[] = array( 
                    'entity_id' => $category->getId(),
                    'name' => $category->getName(),
                    'label' => $category->getData('category_label'),
                    'title' => $category->getData('category_title'),
                    // 'description' => $category->getDescription(),
                    // 'short_description' => $category->getCategoryShortDescription(),
                    'url' => $category->getUrl(),
                    'url_key' => $category->getUrlKey(),
                    'url_path' => $category->getUrlPath(),
                    // 'promo' => $category->getData('category_promo'),
                    'featured_image' => array(
                        'image' => $category->getResizedThumbnailImage(350,250,78),
                        'image_2x' => $category->getResizedThumbnailImage(350,250,78),
                        'image_3x' => $category->getResizedThumbnailImage(700,500,78),
                        'image_webp' => $this->_getWebp($category->getData("sw_ocat_category_icon_image")),
                    ),
                    
                   
                    );
            }
            
             $this->_sendAPI($response);
            
        }

        public function getBrandViewAction(){
            $category_id = $this->_getPathId('category_id');
            $category = Mage::getModel('catalog/category')->load($category_id);
            $seo = 
            '<div class="show-for-medium">
            <h3>Pertanyaanya mengenai ' .$category->getName(). ' yang paling sering ditanya</h3>
            <p>
            Dimana tempat jual '.$category->getName().' original, rekomendasi '.$category->getName().' yang paling bagus, harga '.$category->getName().' di indonesia, belanja '.$category->getName().' dengan harga dan spesifikasi lengkap, '.$category->getName().' garansi resmi, apa perbedaan antara '.$category->getName().' asli dan palsu
            </p>
            </div>';
            $response = array( 
                'entity_id' => $category->getId(),
                //'child_category' => $this->_getChildCategory($category->getId()),
                'name' => $category->getName(),
                'label' => $category->getData('category_label'),
                'title' => $category->getData('category_title'),
                'description' => $category->getDescription() .$seo,
                'short_description' => $category->getCategoryShortDescription()?Mage::helper('core')->stripTags($category->getCategoryShortDescription()) :$this->_metaDescription($category->getName()),
                'url' => $category->getUrl(),
                'url_key' => $category->getUrlKey(),
                'url_path' => $category->getUrlPath(),
                'promo' => $category->getData('category_promo'),
                'featured_image' => array(
                    'image' => $category->getResizedThumbnailImage(200,250,78),
                    'image_2x' => $category->getResizedThumbnailImage(400,500,78),
                    'image_3x' => $category->getResizedThumbnailImage(800,1000,78)
                ),
                'meta_description' => $category->getData('meta_description') ? Mage::helper('core')->stripTags($category->getData('meta_description')) : $this->_metaDescription($category->getName()),
                'meta_title' => $category->getData('meta_title') ? $category->getData('meta_title') : $this->_metaTitle($category->getName()),
                'products' => $this->_getProducts($category_id),  
            );
            
            $this->_sendAPI($response);
            
        }

        public function _metaTitle($title){
            $title = 'JUAL '.$title.' | Authorized Online Retailer | Wearinasia';
            return $title;
        }
        public function _metaDescription($title){
            $description = 'Dapatkan '.$title.' informasi pembelian, harga dan spesifikasi selengkapnya. Beli dengan cicilan 0% dan gratis ongkos kirim di Wearinasia';
            return $description;
        }




         // CATEGORY

         public function getCategorysAction(){
           
            $parrent_category = Mage::getModel('catalog/category')->load(659);
            $childCategorys = $parrent_category->getChildrenCategories();

            foreach($childCategorys as $childCategory){
                $category = Mage::getModel('catalog/category')->load($childCategory->getId());
                $response[] = array( 
                    'entity_id' => $category->getId(),
                    'name' => $category->getName(),
                    'label' => $category->getData('category_label'),
                    'title' => $category->getData('category_title'),
                  //  'description' => $category->getDescription(),
                  //  'short_description' => $category->getCategoryShortDescription(),
                    'url' => $category->getUrl(),
                    'url_key' => $category->getUrlKey(),
                    'url_path' => $category->getUrlPath(),
                    'promo' => $category->getData('category_promo'),
                    'featured_image' => array(
                        'image' => $category->getResizedThumbnailImage(50,50,78),
                        'image_2x' => $category->getResizedThumbnailImage(400,400,78),
                       // 'image_3x' => $category->getResizedThumbnailImage(800,800,78),
                       
                    ),  
                );
            }
            
             $this->_sendAPI($response);
            
        }


        public function getCategoryViewAction(){
            $category_id = $this->_getPathId('category_id');
            $category = Mage::getModel('catalog/category')->load($category_id);
            if($category->getParentCategory()->getId() == '198'){
                $type='editors_pick';
            }
            else {
                $type = "";
            }

           
            $response = array( 
                'entity_id' => $category->getId(),
                'type' => $type,
                //'child_category' => $this->_getChildCategory($category->getId()),
                'name' => $category->getName(),
                'label' => $category->getData('category_label'),
                'title' => $category->getData('category_title'),
                'description' => $category->getDescription(),
                'short_description' => $category->getCategoryShortDescription()?$category->getCategoryShortDescription():$this->_metaDescription($category->getName()),
                'url' => $category->getUrl(),
                'url_key' => $category->getUrlKey(),
                'url_path' => $category->getUrlPath(),
                'promo' => $category->getData('category_promo'),
                'featured_image' => array(
                    'image' => $category->getResizedThumbnailImage(200,250,78),
                    'image_2x' => $category->getResizedThumbnailImage(400,500,78),
                    'image_3x' => $category->getResizedThumbnailImage(800,1000,78)
                ),
                'featured_category_id' => $category->getData('featured_category_id'),
                'meta_description' => $category->getData('meta_description') ? Mage::helper('core')->stripTags($category->getData('meta_description')) : $this->_metaDescription($category->getName()),
                'meta_title' => $category->getData('meta_title') ? $category->getData('meta_title') : $this->_metaTitle($category->getName()),
                'products' => $this->_getProducts($category_id),
            );
            
            $this->_sendAPI($response);
            
        }

        //PRODUCT VIEW
        public function getProductViewAction(){
            
            $product_id = $this->_getPathId('product_id');
          
            $product = Mage::getModel('catalog/product')->load($product_id);
            if($product->getTypeId() == 'configurable'){
                $productAttributeOptions = $product->getTypeInstance(true)->getConfigurableAttributesAsArray($product);
            }

            if($product->getNonPromoItem() == 1){
                $is_promo = 0;
            } else {
                $is_promo = 1;
            }

           
            
            if($product->getStatus() == 1 ){
               $response = array( 
                    'entity_id' => $product->getId(),
                    'type_id' => $product->getTypeId(),
                    'url' => $product->getProductUrl(),
                    'url_path' => $product->getUrlPath(),
                    'url_key' => $product->getUrlKey(),
                    'brand' => $product->getAttributeText("brand"),
                    'brand_id' => $product->getBrand(),
                    'name' => $product->getName(),
                    'price' => $product->getPrice(),
                    'final_price' => $product->getFinalPrice(),
                    'special_price' => $product->getSpecialPrice(),
                    // 'stock' => json_decode($product->getStockItem()),
                    // 'stock_item' => $product->getStockItem()->toArray(),
                    'images' => $this->_getProductImages($product->getId()),
                    'is_in_stock' => $product->getIsInStock(),
                    'brand_url' => $product->getBrandUrl(),
                    'installment' => $this->_getInstallment($product->getInstallment()),
                    'short_description' => Mage::helper("cms")->getBlockTemplateProcessor()->filter($product->getShortDescription()),             
                    'description' => Mage::helper("cms")->getBlockTemplateProcessor()->filter($product->getDescription()),  
                    'video' => $product->getVideo(),
                    'specification' => $product->getSpesifikasiTeknis(),
                    'warranty' => $product->getWarranty(),
                    'faq' => $product->getFaq(),
                    'in_the_box' => $product->getInTheBox(),
                    'overview' => $product->getTechOverview()?$product->getTechOverview():$product->getAccesoriesSpesifikasiTeknis()?$product->getAccesoriesSpesifikasiTeknis():$product->getFootwearSpesifikasiTeknis(),
                    'is_presale' => $product->getPresale(),
                    'presale_pricelist' => $product->getPresalePricelist(),
                    'options' => $productAttributeOptions,  
                    'is_promo' => $is_promo,
                    'bundle_product' =>  $product->getFreeProductId()?$this->_getBundleProductById($product->getFreeProductId()):null,
                    'flashdeal_in_minutes' => $product->getFlashdealInMinutes(),
                    'meta_description' => $product->getData('meta_description') ? Mage::helper('core')->stripTags($product->getData('meta_description')) : $this->_metaDescription($product->getName()),
                    'meta_title' => $product->getData('meta_title') ? $product->getData('meta_title') : $this->_metaTitle($product->getName()) ,
                    'sku' => $product->getSku(),
                    'related_product'=> $this->_getRelatedProduct($product),
                    'featured_brand_ids'=> $this->_getFeaturedBrand(),
                    'review'=>$this->_getReviewByProductId($product->getId())
                    
                );
               $this->_sendAPI($response);
               
               // $this->_sendAPI($product);
            }
           
            $this->_sendAPI($response);
            
            
        }

        public function _getRelatedProduct($_product){
          //  $_product = Mage::getModel('catalog/product')->load(4693); 
            $relatedProducts = $_product->getRelatedProductIds();
            if($relatedProducts){

                $items[] = array( 
                    'entity_id' => $_product->getId(),
                    'url' => $_product->getProductUrl(),
                    'url_path' => $_product->getUrlPath(),
                    'url_key' => $_product->getUrlKey(),
                    'name' => $_product->getName(),
                    'final_price' => $_product->getFinalPrice(),
                    'image' => (string)Mage::helper('catalog/image')->init($_product, 'small_image')->resize(150,150),
                   
                );

                foreach ($relatedProducts as $relatedProduct){

                    $product = Mage::getModel('catalog/product')->load($relatedProduct);
                    if($product->getVisibility() != 1){
                        continue;
                    }


                    $items[] = array( 
                        'entity_id' => $product->getId(),
                        'url' => $product->getProductUrl(),
                        'url_path' => $product->getUrlPath(),
                        'url_key' => $product->getUrlKey(),
                        'name' => $product->getName(),
                        'final_price' => $product->getFinalPrice(),
                        'image' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(150,150),
                       
                    );
                   
                }

                return  $items;

            }
            else{
                return null;
            }
            
        }

        public function _getFeaturedBrand(){
            return '312,778,378';
        }

        public function _getReviewByProductId($productId ){
           
           
            $reviews = Mage::getModel('review/review')
                ->getResourceCollection()
                ->addStoreFilter(Mage::app()->getStore()->getId())
                ->addEntityFilter('product', $productId)
                ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED)
                ->setDateOrder()
                ->addRateVotes();

                foreach ($reviews->getItems() as $review) {
                    foreach( $review->getRatingVotes() as $vote ) {
                       $value[] =  $vote->getValue();
                    }
                }

               
                $response = array(
                    'value'=>array_sum($value)/$reviews->getSize()?array_sum($value)/$reviews->getSize():5,
                    'count'=>$reviews->getSize()?$reviews->getSize():1,
                );
                return $response;
           
        }

        

        public function _getBundleProductById($id){
            $product = Mage::getModel('catalog/product')->load($id);
            $response = array( 
                'entity_id' => $product->getId(),
                'url' => $product->getProductUrl(),
                'url_path' => $product->getUrlPath(),
                'url_key' => $product->getUrlKey(),
                'name' => mb_strimwidth($product->getName(), 0, 40, "..."),
                'images' => $this->_getProductImages($product->getId()),
               
            );
            return $response;
        }

        public function getRelatedProductsAction(){
            $id = $this->getRequest()->getParam('id');
            $category = Mage::getModel('catalog/category')->load(121);

            $response = array( 
                'entity_id' => $category->getId(),
                'name' => $category->getName(),
                'label' => $category->getData('category_label'),
                'title' => $category->getData('category_title'),
                'url' => $category->getUrl(),
                'url_key' => $category->getUrlKey(),
                'url_path' => $category->getUrlPath(),
                'products' => $this->_getProducts($category_id),  
            );
            
            $this->_sendAPI($id );
            
        }

        public function getChildbyProductAction(){
            $id = $this->getRequest()->getParam('id');

            $count = 0;
            $product = Mage::getModel('catalog/product')->load($id);
            foreach($product->getCategoryIds() as $categoryId){
                $category = Mage::getModel('catalog/category')->load($categoryId);
                foreach ($category->getParentCategories() as $parent) {
                    if($parent->getName() == $category->getName()){ continue; }
                    else {
                        if($parent->getId() == 756){
                            $response[] = array( 
                                'entity_id' => $category->getId(),
                                'name' => $category->getName(),
                                'label' => $category->getData('category_label'),
                                'title' => $category->getData('category_title'),
                                'url' => $category->getUrl(),
                                'url_key' => $category->getUrlKey(),
                                'url_path' => $category->getUrlPath(),
                                'promo' => $category->getData('category_promo'),
                                'promo_end_date' => Mage::getModel('core/date')->date('d.m.Y', strtotime($category->getData('promo_end_date'))),
                                'campaign_progress' =>  'Rp 31982381',
                                'campaign_participant' =>  '5 Person',
                                'campaign_donation' => 'Rp. 50000',
                                'profile_image' => array(
                                    'image' => $category->getResizedImageHelper( $category->getData('menu_menu_icon'),200,200,78),
                                    'image_2x' => $category->getResizedImageHelper( $category->getData('menu_menu_icon'), 400,400,78),
                                    
                                ),
                            );
                        }
                        else { continue; }
                    }
                }
                if($response == null){
                    $response = [];
                }
                $print = array(
                    "campaign" => $response
                );
            }
    
            $this->_sendAPI($print);
        }

        public function getUpSellsProductAction(){
            $id = $this->getRequest()->getParam('id');
            $product = Mage::getModel('catalog/product')->load($id);
            // Get all related product ids of $product_id.
            $allRelatedProductIds = $product->getUpSellProductCollection();

            if(count($product->getUpSellProductIds()) == 0){
                $products = Mage::getModel("catalog/product")
                    ->getCollection()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('brand', $product->getBrand())
                    ->joinField('is_in_stock',
                        'cataloginventory/stock_item',
                        'is_in_stock',
                        'product_id=entity_id',
                        'is_in_stock=1',
                        '{{table}}.stock_id=1',
                        'left')
                    ->load();

                if($products != null){
                    foreach($products as $product){
                        if(!empty($product->getPromoId())){
                            $promo = $this->_getPromoById($product->getPromoId());
                        }
                        
                        $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product->getId());
			

                        if($parentIds == null && $product->getId() != $id){
                            $array[] = array(
                                    'entity_id' => $product->getId(),
                                    'url' => $product->getProductUrl(),
                                    'url_path' => $product->getUrlPath(),
                                    'url_key' => $product->getUrlKey(),
                                    'brand' => $product->getAttributeText("brand"),
                                    'brand_id' => $product->getBrand(),
                                    'name' => mb_strimwidth($product->getName(), 0, 40, "..."),
                                    'price' => $product->getPrice(),
                        'final_price' => $product->getFinalPrice(),
                        'special_price' => $product->getSpecialPrice(),
                                    'promo' => $promo,
                                    'is_in_stock' => $product->getStockItem()->getIsInStock(),
                                    'featured_image' => array(
                                        'image' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(100,75),
                                        'image_2x' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(200,150),
                                        'image_3x' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(400,300),
                                        // 'image_4x' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(800,600),
                                    ),
                                    // 'description' => $product->getDescription(),
                                    'brand_url' => $product->getBrandUrl(),
                                    'installment' => $this->_getInstallment($product->getInstallment()),
                                    'is_promo' => $product->getNonPromoItem(),
                                    'parent' => $product->getParentItemId()
                                    // 'short_description' => $product->getShortDescription(),
                                    // 'description' => $product->getDescription(),
                                );
                        }
                        else {}
                    }
                    if($array != null){
                        $message_dialog = "Get Brand Products List Success";
                        $message_code = 200;
                    }
                    else {
                        $message_dialog = "Failed to Get Brand Products List";
                        $message_code = 400;
                    }
                }
                else {
                    $message_dialog = "Brand product not in stock";
                    $message_code = 400;
                }
            }
            else {
                foreach ($product->getUpSellProductCollection() as $id) {
                    $ids[] = array(
                        'product_id' => $id->getId()
                    );
                }

		        $products = Mage::getModel("catalog/product")
                    ->getCollection()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('entity_id', array('in', $ids))
                    ->joinField('is_in_stock',
                        'cataloginventory/stock_item',
                        'is_in_stock',
                        'product_id=entity_id',
                        'is_in_stock=1',
                        '{{table}}.stock_id=1',
                        'left')
                    ->load();

                if($products != null){
                    foreach ($products as $product){
                        if(!empty($product->getPromoId())){
                            $promo = $this->_getPromoById($product->getPromoId());
                        }
    
                        $array[] = array(
                            'entity_id' => $product->getId(),
                            'url' => $product->getProductUrl(),
                            'url_path' => $product->getUrlPath(),
                            'url_key' => $product->getUrlKey(),
                            'brand' => $product->getAttributeText("brand"),
                            'brand_id' => $product->getBrand(),
                            'name' => mb_strimwidth($product->getName(), 0, 40, "..."),
                            'price' => $product->getPrice(),
                            'final_price' => $product->getFinalPrice(),
                            'special_price' => $product->getSpecialPrice(),
                            'promo' => $promo,
                            'is_in_stock' => $product->getStockItem()->getIsInStock(),
                            'featured_image' => array(
                                'image' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(100,75),
                                'image_2x' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(200,150),
                                'image_3x' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(400,300),
                                // 'image_4x' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(800,600),
                            ),
                            // 'description' => $product->getDescription(),
                            'brand_url' => $product->getBrandUrl(),
                            'installment' => $this->_getInstallment($product->getInstallment()),
                            // 'short_description' => $product->getShortDescription(),
                            // 'description' => $product->getDescription(),
                        );
                    }
                    
                    if($array != null){
                        $message_dialog = "Get Up-sells Products List Success";
                        $message_code = 200;
                    }
                    else {
                        $message_dialog = "Failed to Get Up-sells Products List";
                        $message_code = 400;
                    }
                }
                else {
                    $message_dialog = "Up-sells product not in stock";
                    $message_code = 400;
                }
            }
            $print = array(
                'message_code' => $message_code,
                'message_dialog' => $message_dialog,
                'product_list' => $array
            );
            $this->_sendAPI($print);
        }

        public function getProductbyBrandAction(){

            $categorys = Mage::getResourceModel('catalog/category_collection');
            $categorys->addAttributeToSelect('*');
	        $categorys->addAttributeToFilter('homepage_f_brand', 1);
            $categorys->setCurPage(1)->setPageSize(1);
            $categorys->load();
            
            $category = $categorys->getFirstItem();
            $response = array( 
                'entity_id' => $category->getId(),
                'name' => $category->getName(),
                'label' => $category->getData('category_label'),
                'title' => $category->getData('category_title'),
                // 'description' => $category->getDescription(),
               // 'short_description' => mb_strimwidth($category->getCategoryShortDescription(), 0, 80, "..."),
                'url' => $category->getUrl(),
                'url_key' => $category->getUrlKey(),
                'url_path' => $category->getUrlPath(),
                // 'promo' => $category->getData('category_promo'),
                'featured_image' => array(
                    'image' => $category->getResizedThumbnailImage(50,50,78),
                    'image_2x' => $category->getResizedThumbnailImage(350,350,78),
                    'image_3x' => $category->getResizedThumbnailImage(600,600,78)
                ),
                'products' => $this->_getProducts($category->getId()),
            );
            $this->_sendAPI($response);
        }

        public function getVoucherAction(){
            $params = $this->getRequest()->getParams();
            $productid = $params['product_id'];
            
            $coll = Mage::getResourceModel('salesrule/rule_collection')
                ->addFieldToFilter('is_rss', 1)
                ->load();
            
            $product = Mage::getModel("catalog/product")->load($productid);
                foreach($coll as $rule) {
                    if ($rule->getActions()->validate($product)){
                        $promo = Mage::getModel('salesrule/rule')->load($rule->getId());
                        if(!empty($promo->getCouponCode())){
                            $array[] = array(
                                'name' => $promo->getName(),
                                'description' => $promo->getDescription(),
                                'code' => $promo->getCouponCode(),
                                'from_date' => $promo->getFromDate(),
                                'to_date' => $promo->getToDate(),
                                'simple_action' => $promo->getSimpleAction(),
                                'discount_amount' => $promo->getDiscountAmount()
                            );
                        } 
                    }
                }
                if(!empty($array)){
                    $message_code = array( 
                        'message_code' => 200,
                        'message_dialog' => 'Get Promo Codes Success',
                        'promo' => $array
                    );
                }
                else {
                    $message_code = array( 
                        'message_code' => 400,
                        'message_dialog' => 'No Promo Code',
                        'promo' => $array
                    );
                }
                $this->_sendAPI($message_code);
        }

        public function getAllVoucherAction(){
            
            $coll = Mage::getResourceModel('salesrule/rule_collection')
                ->addFieldToFilter('is_rss', 1)
                ->load();

            $time = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $today = date('Y-m-d H:i:s', strtotime($time. ' + 7 hours'));
            
            // $product = Mage::getModel("catalog/product")->load($productid);
                foreach($coll as $rule) {
                    // if ($rule->getActions()->validate($product)){
                        $promo = Mage::getModel('salesrule/rule')->load($rule->getId());
                        if(!empty($promo->getCouponCode())){
                            if($promo->getToDate() < $today){
                                continue;
                            }
                            else {
                                $array[] = array(
                                    'name' => $promo->getName(),
                                    'description' => $promo->getDescription(),
                                    'code' => $promo->getCouponCode(),
                                    'from_date' => $promo->getFromDate(),
                                    'to_date' => $promo->getToDate(),
                                    'simple_action' => $promo->getSimpleAction(),
                                    'discount_amount' => $promo->getDiscountAmount()
                                );
                            }
                        } 
                    // }
                }
                if(!empty($array)){
                    $message_code = array( 
                        'message_code' => 200,
                        'message_dialog' => 'Get Promo Codes Success',
                        'promo' => $array
                    );
                }
                else {
                    $message_code = array( 
                        'message_code' => 400,
                        'message_dialog' => 'No Promo Code',
                        'promo' => $array
                    );
                }
                $this->_sendAPI($message_code);
        }

        public function getVoucherDetailsAction(){

            $params = $this->getRequest()->getParams();
            $code = $params['code'];

            $oCoupon = Mage::getModel('salesrule/coupon')->load($code, 'code');
            $promo = Mage::getModel('salesrule/rule')->load($oCoupon->getRuleId());
            $time = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
            $today = date('Y-m-d H:i:s', strtotime($time. ' + 7 hours'));
                if(!empty($promo->getCouponCode())){
                    if($promo->getToDate() < $today){
                    }
                    else {
                        $array = array(
                            'name' => $promo->getName(),
                            'description' => $promo->getDescription(),
                            'code' => $promo->getCouponCode(),
                            'from_date' => $promo->getFromDate(),
                            'to_date' => $promo->getToDate(),
                            'simple_action' => $promo->getSimpleAction(),
                            'discount_amount' => $promo->getDiscountAmount()
                        );
                    }
                }
                if(!empty($array)){
                    $message_code = array( 
                        'message_code' => 200,
                        'message_dialog' => 'Get Promo Codes Success',
                        'promo' => $array
                    );
                }
                else {
                    $message_code = array( 
                        'message_code' => 400,
                        'message_dialog' => 'No Promo Code',
                        'promo' => $array
                    );
                }
                $this->_sendAPI($message_code);
        }


        //FEATURED BLOCK
        public function getFeaturedJournalAction(){
            
            $posts = Mage::getResourceModel('wordpress/post_collection')
            ->addPostTypeFilter('post')
            ->setOrderByPostDate()
            ->addIsViewableFilter()
            ->setPageSize(2)
            ->load();

            foreach($posts  as $post){
                $response[] = array( 
                    'author' => $post->getAuthor()->getDisplayName(),
                    'date' => $post->getPostDate(),
                    'title' => mb_strimwidth($post->getPostTitle(), 0, 50, "..."),
                    
                    'url' => $post->getPermalink(),
                    'image' => $post->getFeaturedImage()->getThumbnailImage(),
                    'author_image' => $post->getAuthor()->getGravatarUrl('80')
                    
                   
                );
            }
            
            
             $this->_sendAPI($response);
   
            
        }

        public function getChildCategoryByIdAction(){
            $category_id = $this->getRequest()->getParam('id');       
            $parrent_category = Mage::getModel('catalog/category')->load($category_id);
            $childCategorys = $parrent_category->getChildrenCategories();

            foreach($childCategorys as $childCategory){
                $category = Mage::getModel('catalog/category')->load($childCategory->getId());
                $response[] = array( 
                    'name' => $category->getName(),
                    'label' => $category->getData('category_label'),
                    'title' => $category->getData('category_title'),
                    'description' => $category->getCategoryShortDescription(),
                    'url' => $category->getUrl(),
                    'url_key' => $category->getUrlKey(),
                    'url_path' => $category->getUrlPath(),
                    'promo' => $category->getData('category_promo'),
                    'image' => $category->getResizedImage(100,125,78),
                    'image_2x' => $category->getResizedImage(400,500,78),
                    'image_3x' => $category->getResizedImage(800,1000,78),
                    'image_4x' => $category->getResizedImage(1600,2000,78),
                   
                );
            }
            
            
             $this->_sendAPI($response);
            
        }

        public function getCategoryByIdsAction(){
            $category_id = $this->getRequest()->getParam('id');  
            $terms = explode(',',  $category_id);   
            
            foreach($terms as $term){
                $category = Mage::getModel('catalog/category')->load($term);
                $name = $category->getData('category_title');
                $cat_name = $category->getData('category_title');

                $response[] = array( 
                    'name' => $category->getName(),
                    'label' => $category->getData('category_label'),
                    'title' => mb_strimwidth($cat_name, 0, 40, "..."),
                    'description' => mb_strimwidth($name, 0, 40, "..."),
                    'url' => $category->getUrl(),
                    'url_key' => $category->getUrlKey(),
                    'url_path' => $category->getUrlPath(),
                    'promo' => $category->getData('caategory_promo'),
                    'image' => $category->getResizedThumbnailImage(175,125,50),
                    'image_2x' => $category->getResizedThumbnailImage(350,250,78),
                    'image_3x' => $category->getResizedThumbnailImage(350,250,78),
                    
                   
                );
            }


            
            
            
            $this->_sendAPI($response);
            
        } 
        

        public function PostAlertAction(){
            // $email = Mage::getModel('productalert/email')->getCollection()->load();
            // $customer_stock_alerts = Mage::getModel('productalert/stock')
            //     ->getCollection();
            // $customer_price_alerts = Mage::getModel('productalert/price')
            //     ->getCollection();
            // $this->_sendAPI($customer_price_alerts);

            $params = $this->getRequest()->getParams();
            $product = $params['entity_id'];

            if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                if(!empty($product)){
                    try {
                        $model = Mage::getModel('productalert/stock')
                            ->setCustomerId(Mage::getSingleton('customer/session')->getId())
                            ->setProductId($product)
                            ->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
                        $model->save();
                        $message_code = array('message_code' => 200, 'message_dialog' => 'Send Alert Success', 'id' => $model->getId());
                        $this->_sendAPI($message_code);
                    }
                    catch (Exception $e) {
                        $message_code = array( 
                            'message_code' => 400,
                            'message_dialog' => 'Alert cannot be create, '.$e->getMessage()
                        );
                        Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
                    }
                }
                else {
                    $message_code = array('message_code' => 400, 'message_dialog' => 'Entity ID is null');
                    $this->_sendAPI($message_code);
                }
            }
            else {
                $message_code = array('message_code' => 405, 'message_dialog' => 'Method Not Allowed');
                $this->_sendAPI($message_code);
            }
        }
    }

?>
