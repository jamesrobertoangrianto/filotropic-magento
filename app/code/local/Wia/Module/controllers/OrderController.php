<?php
    class Wia_Module_OrderController extends Mage_Core_Controller_Front_Action
    {

        public function indexAction(){

            $productId = 11;
            $products = Mage::getModel('catalog/product')->load($productId);
            // $childIds = Mage::getModel('catalog/product_type_configurable')->getChildrenIds($product->getId());
            $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$products);
            foreach ($childProducts as $product){
                $response[]=$product->toArray();
            }
            $this->_sendAPI($response);

           

        //    try {
        //     $product_id = 2085;
        //     $product = Mage::getModel('catalog/product')->load($product_id);
        //     $cart = Mage::getModel('checkout/cart');
        //     $cart->init();
        //     $options = array(4 => 3, 5 => 1);
        //     $params = array(
        //         'product_id' => $product_id,
        //         'qty' => 1,
        //         'options' => $options
        //     );
        
        //     $cart->addProduct($product, $params);
        //     $cart->save();
        //     Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
        //   } catch (Exception $e) {
        //     print_r($e->getMessage());
        //   }
     

          

          
            // $coll = Mage::getResourceModel('salesrule/rule_collection')
            // ->addFieldToFilter('is_rss', 1)
            // ->load();
            
            //     $items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
            //     foreach($items as $item)
            //     { 
            //         //echo $item->getProductId();

            //         foreach($coll as $rule) {
            //             if ($rule->getActions()->validate($item)){
            //                 echo $rule->getId();
            //                 $promo = Mage::getModel('salesrule/rule')->load($rule->getId());
            //                echo $promo->getCouponCode();
            //             }
                    
            //         }
                    
            //     }

            // $curl = curl_init();

            // curl_setopt_array($curl, array(
            //   CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
            //   CURLOPT_RETURNTRANSFER => true,
            //   CURLOPT_ENCODING => "",
            //   CURLOPT_MAXREDIRS => 10,
            //   CURLOPT_TIMEOUT => 30,
            //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            //   CURLOPT_CUSTOMREQUEST => "POST",
            //   CURLOPT_POSTFIELDS => "origin=501&originType=city&destination=574&destinationType=subdistrict&weight=1700&courier=jne",
            //   CURLOPT_HTTPHEADER => array(
            //     "content-type: application/x-www-form-urlencoded",
            //     "key: 7f4fbbf9395597d08fe45f110e867332"
            //   ),
            // ));
            
            // $response = curl_exec($curl);
            // $err = curl_error($curl);
            
            // curl_close($curl);
            
            // if ($err) {
            //   echo "cURL Error #:" . $err;
            // } else {
            //   echo $response;
            // }

            

        }


        public function getVoucherListAction(){

           
     
            $coll = Mage::getResourceModel('salesrule/rule_collection')
            ->addFieldToFilter('is_rss', 1)
            ->load();
            
                $items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
                foreach($items as $item)
                { 
                    //echo $item->getProductId();

                    foreach($coll as $rule) {
                        if ($rule->getActions()->validate($item)){
                          
                            $promo = Mage::getModel('salesrule/rule')->load($rule->getId());
                            if(!empty( $promo->getCouponCode())){
                                echo '<div class="flex-container promo-containers">';
                                
                                echo '<div class="flex-item flex-3 promo-name">
                                    <div class="promo-desc">'.$promo->getDescription().'</div>
                                    <div>Promo Code : <b>'.$promo->getCouponCode().'</b></div>
                                </div>';
                                
                                echo '</div>';
                           
                            }
                           
                        }
                    
                    }
                    
                }

            

        }

        public function _sendAPI($data){
            $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));

           
        }
        
        

        protected function _getCart()
        {
            return Mage::getSingleton('checkout/cart');
            
        }

        protected function _getSession()
        {
            return Mage::getSingleton('checkout/session');
        }

    
        protected function _getQuote()
        {
            return $this->_getCart()->getQuote();
        }

        protected function _clearsession(){
            // Clear Magento cart and quote items
            $cart = Mage::getModel('checkout/cart');                
            $cart->truncate()->save(); // remove all active items in cart page
            $cart->init();
            $session= Mage::getSingleton('checkout/session');
            $quote = $session->getQuote();
            $cart = Mage::getModel('checkout/cart');
            $cartItems = $cart->getItems();
            foreach ($cartItems as $item)
            {
                $quote->removeItem($item->getId())->save();
            }
            Mage::getSingleton('checkout/session')->clear();
                        
        }

    

        protected function _initProduct()
        {
            $productId =  (int) $this->getRequest()->getParam('product');
            if ($productId) {
                $product = Mage::getModel('catalog/product')
                    ->setStoreId(Mage::app()->getStore()->getId())
                    ->load($productId);
                if ($product->getId()) {
                    return $product;
                }
            }
            return false;
        }


        public function cartAction(){
            $cart = Mage::helper('checkout/cart');
            $quote = Mage::helper('checkout/cart')->getQuote();
          

            foreach ($quote->getItemsCollection() as $item) {
                if ($item->getParentItemId()) {
                    continue;
                }

                $product = $item->getProduct();
                $helper = Mage::helper('catalog/image');

                $simpleProduct =  $item->getOptionByCode('simple_product')->getProduct();
                $simpleProductId = $simpleProduct->getId();

                

                $image = $product->getSmallImageUrl();

                $thumbnail = (string)Mage::helper('catalog/image')->init($item->getProduct(), 'small_image')->resize(150,200);
                $items[]= array (
                    'id' =>  $product->getTypeId(),
                    'ids' => $simpleProductId ,
                    'name' => $simpleProduct->getName(),
                    'base_image' => $thumbnail,
                    'product_quantity' => $simpleProduct->getStockItem()->getQty() * 1,
                    'price' => $item->getPrice(),
                );
            }
           
            $response = array( 
                'subtotal' => $quote->getSubtotal(),
                'qty' => $cart->getSummaryCount(),
                'cart_items' => $items,
            );

            $this->_sendAPI($response);



        }

        public function DeleteProductAction(){

            $id = (int)$this->getRequest()->getParam('product');
            if ($id) {
                try {
                    $cartHelper = Mage::helper('checkout/cart');
    
                    $items = $cartHelper->getCart()->getItems();
                    
                    $cartHelper->getCart()->removeItem( $id )->save();
                    Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
    
                    $message_code = array( 
                        'message_code' => 200,
                        'message_dialog' => 'Remove Item Success'
                    );
                    $this->_sendAPI($message_code);
                } catch (Exception $e) {
                    $message_code = array( 
                        'message_code' => 400,
                        'message_dialog' => 'Cannot Remove Item'
                    );
                    $this->_sendAPI($message_code);
                }
            }
        }


    public function addtoCartAction(){
        try {
            

            $cart = Mage::getModel('checkout/cart');

            $product = $this->_initProduct();
            $params = $this->getRequest()->getParams();

            $cart->init();
            $cart->addProduct($product, $params);
            $cart->save();
            Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
            
            $qty = Mage::helper('checkout/cart')->getSummaryCount();
            
           
            $quote = Mage::helper('checkout/cart')->getQuote();
            foreach ($quote->getAllItems() as $item) {
                if ($item->getParentItemId()) {
                    continue;
                }
                $items[]= array (
                    'id' => $item->getProductId(),
                    'name' => $item->getName(),
                    'quantity' => $item->getQty(),
                    'price' => $item->getPrice(),
                );
            }
           

            $response = array( 
                'subtotal' => Mage::helper('core')->currency($quote->getSubtotal(), true, false),
                'qty' => $qty,
                'cart_items' => $items,
                
            );


            

            
            $this->_sendAPI($response);


          } catch (Exception $e) {

            $qty = Mage::helper('checkout/cart')->getSummaryCount();
            
           
            $quote = Mage::helper('checkout/cart')->getQuote();
            foreach ($quote->getAllItems() as $item) {
                if ($item->getParentItemId()) {
                    continue;
                }
                $items[]= array (
                    'id' => $item->getProductId(),
                    'name' => $item->getName(),
                    'quantity' => $item->getQty(),
                    'price' => $item->getParentItemId()?  $item->getParentItem()->getPrice(): $item->getPrice()
                );
            }
           

            $response = array( 
                'subtotal' => $quote->getSubtotal(),
                'qty' => $qty,
                'cart_items' => $items,
                
            );

            
              
            print_r($e->getMessage());

          }
    }

       

    //Get shipping address
    public function getShippingAddressAjaxAction(){

        $this->updateShippingAddress();
        
    }



    //Add new shipping addresss for old customer
    public function addAddressAction(){
            
        if (!$this->_validateFormKey()) {
            $this->_redirect('*/*');
            return;
        }

            //login customer data
            $websiteId = Mage::app()->getWebsite()->getId();
            $store = Mage::app()->getStore();
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            
        
            $params = $this->getRequest()->getParams();
            //var_dump($params);
            $firstName = $customer->getFirstname();
            $lastName = $customer->getLastname();
            $region = $params['region_id'];
            $code = $params['postcode'];
            $city = $params['city'];
            $street = $params['street'];
            $location = $params['location'];
            $phone = $params['telephone'];
            $vat = $params['vat_id'];




            $shippingAddress = array(
            'firstname' => $firstName,
            'middlename' => '',
            'lastname' => $lastName,
            'street' => array(
                '0' => $street, // compulsory
                '1' => $location // optional
            ),
            'city' => $city,
            'country_id' => 'ID', // two letters country code
            'region' => 'ds', // can be empty '' if no region
            'region_id' => $region, // can be empty '' if no region_id
            'postcode' => $code,
            'telephone' => $phone,
            'fax' => '',
            'vat_id' => $vat,
            'save_in_address_book' => 1
        );


        $Address = Mage::getModel('customer/address');  
        $Address->setData($shippingAddress)
                                ->setCustomerId($customer->getId())
                                ->setIsDefaultBilling('1')
                                ->setIsDefaultShipping('1')
                                ->setSaveInAddressBook('1');
        $Address->save();

       $this->_redirect('checkout/onepage');
       
      
       

       
       
    }

    public function setShippingAddressAction(){
        // $params = $this->getRequest()->getParams();
        // $code = $params['estimate_method'];
        
            
        $code = $this->getRequest()->getParam('estimate_method'); 
       
        $customer= Mage::getSingleton('customer/session')->getCustomer();
        $customerShippingAddress = $customer->getPrimaryShippingAddress();


        // Update the cart's quote.
        $cart = Mage::getSingleton('checkout/cart');

        $address = $cart->getQuote()->getShippingAddress();
        $address
                ->setShippingMethod($code)
                ->setCollectShippingrates(true);
        $cart->save();
       
        //update ordersummary
        $this->updateOrderSummaryAction();
       

    }



    

    public function updateOrderSummaryAction()
    {
        $layout = $this->getLayout();
        $shippingaddressBlock = $layout->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml');
        echo $shippingaddressBlock->toHtml();

        
    

        
    }
    
      //update shipping address response
    public function updateShippingMethodAction(){
        $layout = $this->getLayout();
        $shippingaddressBlock = $layout->createBlock('checkout/cart_totals')->setTemplate('operationalmodule/shipping-method.phtml');
        echo $shippingaddressBlock->toHtml();

        

           
        
    }

    public function addReviewAction(){
                
                

                $params = $this->getRequest()->getParams();
                
                // $productId = '2085';
                // $rating = 5;
                // $review_title = 'titles';
                // $review_detail = 'hellos';

                $productId = $params['product_id'];
                $rating = $params['rating'];
                $review_title = $params['review_title'];
                $review_detail = $params['review_details'];


          
                $customer = Mage::getSingleton('customer/session')->getCustomer();
                $customerName = $customer->getName();
                $review = Mage::getModel('review/review');
                $review->setEntityPkValue($productId);//product id
                $review->setStatusId(2); // approved
                $review->setTitle($review_title);
                $review->setDetail($review_detail);
                $review->setEntityId(1);                                      
                $review->setStoreId(Mage::app()->getStore()->getId());                    
                $review->setCustomerId($customer->getId());//null is for administrator
                $review->setNickname($customerName);
                $review->setReviewId($review->getId());
                $review->setStores(array(Mage::app()->getStore()->getId()));                    
                $review->save();
                $review->aggregate();

                $rating_options = array(
                    1 => array(1 => 1,  2 => 2,  3 => 3,  4 => 4,  5 => 5),
                    2 => array(1 => 6,  2 => 7,  3 => 8,  4 => 9,  5 => 10),
                    3 => array(1 => 11, 2 => 12, 3 => 13, 4 => 14, 5 => 15),
                    4 => array(1 => 16, 2 => 17, 3 => 18, 4 => 19, 5 => 20),
                );
        
                Mage::getModel('rating/rating')
                ->setRatingId(4)
                ->setReviewId($review->getId())
                ->addOptionVote($rating_options[4][$rating], $productId);
    }
    

    //update shipping address response
    public function updateShippingAddressAction(){
        $layout = $this->getLayout();
        $shippingaddressBlock = $layout->createBlock('checkout/cart_totals')->setTemplate('operationalmodule/shipping-address.phtml');
        echo $shippingaddressBlock->toHtml();      
    }



    //apply voucher code

    public function applyVoucherAction(){
    

        $code = $this->getRequest()->getParam('code');
        Mage::getSingleton('checkout/cart')->getQuote()
        ->setCouponCode($code)->collectTotals()->save();
        $this->updateOrderSummaryAction();

        $rule =  Mage::getSingleton('checkout/session')->getQuote()->getAppliedRuleIds();
        if( $code == 'remove' ){
           
            echo '<div class="callout alert"><p>Promo Code Remove!</p></div>';
        }
        elseif( $rule == 0){
            echo '<div class="callout alert"><p>Promo Code Not Valid!</p></div>';
        }
        else{
            echo '<div class="callout success"><p>Promo Code Applied!</p></div>';
        }

    }

    public function createOrderAction(){
        
        if (!$this->_validateFormKey()) {
            $this->_redirect('/success');
            return;
        }

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();
        $customer= Mage::getSingleton('customer/session')->getCustomer();
        
        $formKeys = Mage::getSingleton('core/session')->getFormKey();

        $shippingMethod =  Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingMethod();
        
        $paymentMethod = 'snapio';

         // Initialize sales quote object
         $quote = Mage::getModel('sales/quote')
         ->setStoreId($store->getId());


        $checkout_session = Mage::getSingleton('checkout/session');
        //$cq = $checkout_session->getQuote();
        //$cq->assignCustomer(Mheage::getSingleton('customer/session')->getCustomer());
        $quote = Mage::getModel('sales/quote')->load($checkout_session->getQuoteId()); // Mage_Sales_Model_Quote
        $quote->getAllItems();                  
        //$quote->reserveOrderId();
        // Set currency for the quote
        $quote->setCurrency(Mage::app()->getStore()->getBaseCurrencyCode()); 

        // Assign customer to quote
        $quote->assignCustomer($customer);

        $billingAddressData = $quote->getBillingAddress();//->addData($customerBillingAddress);
        $shippingAddressData = $quote->getShippingAddress();//->addData($customerShippingAddress);

         // Set shipping and payment method on quote shipping address data
         $shippingAddressData->setShippingMethod($shippingMethod)
         ->setPaymentMethod($paymentMethod);

            // Set payment method for the quote
        $quote->getPayment()->importData(array('method' => $paymentMethod));


            try {
                // Collect totals of the quote
                $quote->collectTotals();
             
                // Save quote
                $quote->save();
                
                // Create Order From Quote
                $service = Mage::getModel('sales/service_quote', $quote);
                $service->submitAll();
                $incrementId = $service->getOrder()->getRealOrderId();
                $orderId = $service->getOrder()->getId();
                
 
                Mage::getSingleton('checkout/session')
                //    ->setQuoteId('123')
                //    ->setLastRealOrderId('123');
                    ->setRealOrderId($incrementId)
                    ->setLastRealOrderId($incrementId);
 
 
                 //set comment
                 $order = Mage::getModel('sales/order')->load($orderId);
                 $order->sendNewOrderEmail();
                 //$order->addStatusHistoryComment('Booking Date : '.$date.'</br>'.'Booking Time '.$time);
                 $order->save();
 
 
 
                //->clearHelperData();
               
                /**
                 * For more details about saving order
                 * See saveOrder() function of app/code/core/Mage/Checkout/Onepage.php
                 */ 
                
                //$result['redirect'] = 'hello';
             $result['success'] = true;
             $result['error'] = false;
             
             $redirecturl = Mage::getBaseUrl().'snapio/payment/redirect';
                 
             
             //$response['response'] = $this->myAjax();
             $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
             $this->getResponse()->setRedirect($redirecturl);
             $this->getResponse()->setBody(
             Mage::helper('core')->jsonEncode($result));
 
                
                 
             } catch (Mage_Core_Exception $e) {
                 //$redirecturl = Mage::getBaseUrl().'snapbarugopay/payment/redirect';
                 $result['success'] = false;
                 $result['error'] = true;
                 $result['error_messages'] = $e->getMessage();    
                 Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                 
                 if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                     Mage::getSingleton('checkout/session')->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
                 } else {
                     $messages = array_unique(explode("\n", $e->getMessage()));
                     foreach ($messages as $message) {
                         Mage::getSingleton('checkout/session')->addError(Mage::helper('core')->escapeHtml($message));
                     }
                 }
             } catch (Exception $e) {
                 $result['success']  = false;
                 $result['error']    = true;
                 $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
                 Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                 
                 Mage::logException($e);
                 //$this->_goBack();
             } 

             $this->_clearsession();
        

    }
    




    }

?>
