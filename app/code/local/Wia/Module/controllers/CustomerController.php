<?php
    class Wia_Module_CustomerController extends Mage_Core_Controller_Front_Action
    {

        public function indexAction(){
           
         
          
        }
        
 
        public function _sendAPI($data){
            $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));

        }

        public function _getCustomerId(){
            $dev = $this->getRequest()->getParam('dev');
            if($dev == 1){
                $customer_id = 194;
            }
            else{
                $customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
            }
            return $customer_id;
        }

        public function _getCustomer(){   
            $customer_id = $this->_getCustomerId();

            if($customer_id){
                $customer = Mage::getModel('customer/customer')->load($customer_id);
                return $customer;
            }
            else{
                return null;
            }
            
        }

         public function _getOrderItems($order){
            $orderItems = $order->getItemsCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('product_type', array('eq'=>'simple'))
                ->load();

                foreach ($orderItems as $item){
                    if ($item->getParentItemId()) {
                        continue;
                    }
                    $optionIds = $item->getOptionByCode('option_ids');
                    if ($optionIds) {
                        $options = array();
                        foreach (explode(',', $optionIds->getValue()) as $optionId) {
                            $option = $product->getOptionById($optionId);
                            if ($option) {
                                $itemOption = $item->getOptionByCode('option_' . $option->getId());
                                $group = $option->groupFactory($option->getType())
                                    ->setOption($option)
                                    ->setConfigurationItem($item)
                                    ->setConfigurationItemOption($itemOption);

                                if ('file' == $option->getType()) {
                                    $downloadParams = $item->getFileDownloadParams();
                                    if ($downloadParams) {
                                        $url = $downloadParams->getUrl();
                                        if ($url) {
                                            $group->setCustomOptionDownloadUrl($url);
                                        }
                                        $urlParams = $downloadParams->getUrlParams();
                                        if ($urlParams) {
                                            $group->setCustomOptionUrlParams($urlParams);
                                        }
                                    }
                                }

                                $options[] = array(
                                    'label' => $option->getTitle(),
                                    'value' => $group->getFormattedOptionValue($itemOption->getValue()),
                                    // 'print_value' => $group->getPrintableOptionValue($itemOption->getValue()),
                                    'option_id' => $option->getId(),
                                    'option_type' => $option->getType(),
                                    'custom_view' => $group->isCustomizedView(),
                                    'price' => $option->getPrice()
                                );
                            }
                        }
                    }
                    
                    $product = $item->getProduct();
                    $response[] = array( 
                        'name'          => $product->getName(),
                        'sku'           => $item->getSku(),
                        'id'           => $item->getId(),
                        'brand'           => $product->getAttributeText("brand"),
                        'price'         => $item->getPrice(),
                        'ordered_qty'   => $item->getQtyOrdered(),
                        'image'         => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(100,100),
                        'custom_options' => $options
                    );
                }

                
                
                
             return   $response;

        }

        public function getOrdersAction(){

            $params = $this->getRequest()->getParams();
            $statuss = $params['status'];
            $status = explode(',',  $statuss);
            $from = date('Y-m-d H:i:s',strtotime("-90 days"));
            $to = date('Y-m-d H:i:s');

            //$customer_id = 23360;
            $customer_id = $this->_getCustomerId();
            $orders = Mage::getResourceModel('sales/order_collection')
            ->addAttributeToFilter('created_at', array('from'=>$from, 'to'=>$to))
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id',  $customer_id)
            ->addFieldToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()))
            ->setOrder('created_at', 'desc');

            if(!empty($status)){
               // $orders->addFieldToFilter('status', $status);
                $orders->addFieldToFilter('status', array($status));
            }
            
            foreach ($orders as $order){
                $items[] = array( 
                    'entity_id' => $order->getId(),
                    'increment_id' => $order->getIncrementId(),
                    'status' => $order->getStatus(),
                    'status_label' => $order->getStatusLabel(),
                    'state' => $order->getState(),
                    'order_date' => $order->getCreatedAt(),
                    'customer_id' => $order->getCustomerId(),
                    'grand_total' => $order->getGrandTotal(),
                    'shipping_method' => $order->getShippingMethod(),
                    'payment_method' => 'Midtrans',
                    'products' =>  $this->_getOrderItems($order),    
                    'shipping_address' => $order->getShippingAddress()->toArray(),                   
                );
            }

            if( $customer_id == null){
                $response = array( 
                    'status_code' => 'not_allowed',
                    'items' => $items,
                );
            }
            else{
                $response = array( 
                    'status_code' => 'allowed',
                    'items' => $items,
                );
            }
            
            $this->_sendAPI($response);

        }

        public function getOrderByIdAction(){
          
            $id = $this->getRequest()->getParam('id');
            // $customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
            $customer_id = $this->_getCustomerId();
            //$customer_id = $this->getRequest()->getParam('customer_id'); 
            $order = Mage::getModel('sales/order')->load($id);
           //$shippings = $order->getShippingAddress()->getShippingMethod();
            $item = array( 
                'entity_id' => $order->getId(),
                'increment_id' => $order->getIncrementId(),
                'status' => $order->getStatus(),
                'state' => $order->getState(),
                'status_label' => $order->getStatusLabel(),
                'order_date' => $order->getCreatedAt(),
                'customer_id' => $order->getCustomerId(),
                'grand_total' => $order->getGrandTotal(),
                'shipping_method' => $order->getShippingDescription(),
                'payment_method' => 'Midtrans',
                'shipping_cost'=> $order->getShippingAmount(),
                'shipping_address' => $order->getShippingAddress()->toArray(),
                'items' =>  $this->_getOrderItems($order),
               
                'tracking'=>$this->_getTrackingByOrder($order),    
            );

            if($order->getCustomerId() == $customer_id){
                $response = array( 
                    'status_code' => 'allowed',
                    'item' => $item
                );
            }
            else{
                $response = array( 
                    'status_code' => 'not_allowed',
                    'item' => null
                );
            }
            

            $this->_sendAPI( $response);
        }

        private function _getTrackingByOrder($order){
           
            $trackNumber = array();
            foreach ($order->getTracksCollection() as $track){
                $tracking[] = array(
                'title' =>$track->getTitle(),
                'number'=> $track->getNumber(),
                );
            }
            return $tracking;
        }

        public function getAddressAction(){
            $customer_id = $this->_getCustomerId();
            $id = $this->getRequest()->getParam('id');
            $customer = Mage::getModel('customer/customer')->load($customer_id);

            foreach ($customer->getAddresses() as $address){


                if($customer->getPrimaryShippingAddress()->getEntityId() == $address->getId()){
                    $primary_address= true;
                }
                else{
                    $primary_address= false;
                }


                //$data = $address->toArray();
                    $items[] = array(
                        'entity_id' => $address->getId(),
                        'customer_id' => $address->getParentId(),
                        'firstname' => $address->getFirstname(),
                        'company' => $address->getCompany(),
                        'city' => $address->getCity(),
                        'region' => $address->getRegion(),
                        'postcode' => $address->getPostcode(),
                        'country_id' => $address->getCountryId(),
                        'telephone' => $address->getTelephone(),
                        'region_id' => $address->getRegionId(),
                        'street' => $address->getStreet(1),
                        'location' => $address->getStreet(2),
                        'vat_id' => $address->getData('vat_id'),
                        'is_shipping_address' => $primary_address,
                    );
                }

                if( $customer_id == null){
                    $response = array( 
                        'status_code' => 'not_allowed',
                        'items' => $items,
                    );
                }
                else{
                    $response = array( 
                        'status_code' => 'allowed',
                        'items' => $items,
                    );
                }
            
            
            
                $this->_sendAPI( $response );
        }

        public function getAddressByIdAction(){
            $customer_id = $this->_getCustomerId();
            $customer = $this->_getCustomer;


            $customer = Mage::getModel('customer/customer')->load($customer_id);
           
            
            
           
            $id = $this->getRequest()->getParam('id');
            if($customer->getPrimaryShippingAddress()->getEntityId() == $id){
                $primary_address= true;
            }
            else{
                $primary_address= false;
            }

          
            $address = Mage::getModel('customer/address')->load($id);
            $item = array(
                'entity_id' => $address->getId(),
                'customer_id' => $address->getParentId(),
                'firstname' => $address->getFirstname(),
                'company' => $address->getCompany(),
                'city' => $address->getCity(),
                'region' => $address->getRegion(),
                'postcode' => $address->getPostcode(),
                'country_id' => $address->getCountryId(),
                'telephone' => $address->getTelephone(),
                'region_id' => $address->getRegionId(),
                'street' => $address->getStreet(1),
                'location' => $address->getStreet(2),
                'vat_id' => $address->getData('vat_id'),
                'is_shipping_address' => $primary_address,
            );

            if($address->getParentId() ==  $customer_id ){
                $response = array( 
                    'status_code' => 'allowed',
                    'item' => $item
                );
            }
            else{
                $response = array( 
                    'status_code' => 'not_allowed',
                    'item' => null
                );
            }
            //var_dump($customer);
            
           $this->_sendAPI($response);
        }
        
        public function setAddressByIdAction(){
            if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
                
                $id = $this->getRequest()->getParam('id');
                $customer_id = $this->_getCustomerId();
                $address = Mage::getModel('customer/address')->load($id);
                if($address->getParentId() ==  $customer_id ){
                    $getraws = $this->getRequest()->getRawBody();
                    $params = json_decode($getraws, true);
                    $customerCompany = $params['company'];
                    $customerTelephone = $params['telephone'];
                    $customerCity = $params['city'];
                    $customerRegion = $params['region'];
                    $customerRegionId = $params['region_id'];
                    $customerPostalcode = $params['postcode'];
                    $customerCountryId = $params['country_id'];   
                    $customerStreet = $params['street'];
                    $location = $params['location'];
                    $vat = $params['vat_id']; 
                    $street = array(
                        '0' => $customerStreet, // compulsory
                        '1' => $location // optional
                    );

                    $address->setCountryId($customerCountryId);
                    $address->setPostcode($customerPostalcode);
                    $address->setCity($customerCity);
                    $address->setTelephone($customerTelephone);
                    $address->setCompany($customerCompany);
                    $address->setStreet($street);
                    $address->setRegion($customerRegion);
                    $address->setRegionId($customerRegionId);
                    $address->setData('vat_id', $vat);
                    $address->save();
                    $response = array( 
                        'status_code' => 'success',
                    );
                }
                else{
                    $response = array( 
                        'status_code' => 'not_allowed',
                    );
                } 
                $this->_sendAPI($response);
            }
            else {
                $response = array( 
                    'status_code' => 'method_not_allowed',
                
                );
                $this->_sendAPI($response);
            }
        }

        public function addAddressByIdAction(){
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                
                    $customer_id = $this->_getCustomerId();
                    $customer = $this->_getCustomer();

                   
                    $getraws = $this->getRequest()->getRawBody();
                    $params = json_decode($getraws, true);
                    if(!$customer){
                        $status = $this->_createNewAccount($params['email'],$params['firstname'],$params['firstname']);
                       
                        if($status){
                            $customer = $this->_getCustomer();
                          
                            $customerCompany = $params['company'];
                            $customerTelephone = $params['telephone'];
                            $customerCity = $params['city'];
                            $customerRegion = $params['region'];
                            $customerRegionId = $params['region_id'];
                            $customerPostalcode = $params['postcode'];
                            $customerCountryId = $params['country_id'];   
                            $customerStreet = $params['street'];
                            $location = $params['location'];
                            $vat = $params['vat_id']; 
                            $street = array(
                                '0' => $customerStreet, // compulsory
                                '1' => $location // optional
                            );
        
                            
        
                            $address = Mage::getModel('customer/address');  
                            $address->setPostcode($customerPostalcode);
                            $address->setCity($customerCity);
                            $address->setTelephone($customerTelephone);
                            $address->setCompany($customerCompany);
                            $address->setStreet($street);
                            $address->setRegion($customerRegion);
                            $address->setRegionId($customerRegionId);
                            $address->setData('vat_id', $vat);
                            $address->setCustomerId($customer->getId())
                                ->setFirstname($customer->getFirstname())
                                ->setLastname($customer->getLastname())
                                ->setCountryId('ID')
                                ->setIsDefaultBilling('1')
                                ->setIsDefaultShipping('1')
                                ->setSaveInAddressBook('1');
                            $address->save();
                            
                            $response = array( 
                                'status_code' => '200',
                                'message_dialog' => 'success',
                                'entity_id' => $address->getId(),
                                // 'customer_id' => $address->getParentId(),
                            );
                        
                       
                            $this->_sendAPI($response);
                           

                        }
                        else{
                            $response = array( 
                                'status_code' => '400',
                                'message_dialog' => 'customer_exist',
                            ); 
                            $this->_sendAPI($response);
                           
                        }
                        
                    }
                    else{
                       
                        $customerCompany = $params['company'];
                        $customerTelephone = $params['telephone'];
                        $customerCity = $params['city'];
                        $customerRegion = $params['region'];
                        $customerRegionId = $params['region_id'];
                        $customerPostalcode = $params['postcode'];
                        $customerCountryId = $params['country_id'];   
                        $customerStreet = $params['street'];
                        $location = $params['location'];
                        $vat = $params['vat_id']; 
                        $street = array(
                            '0' => $customerStreet, // compulsory
                            '1' => $location // optional
                        );
    
                        
    
                        $address = Mage::getModel('customer/address');  
                        $address->setPostcode($customerPostalcode);
                        $address->setCity($customerCity);
                        $address->setTelephone($customerTelephone);
                        $address->setCompany($customerCompany);
                        $address->setStreet($street);
                        $address->setRegion($customerRegion);
                        $address->setRegionId($customerRegionId);
                        $address->setData('vat_id', $vat);
                        $address->setCustomerId($customer->getId())
                            ->setFirstname($customer->getFirstname())
                            ->setLastname($customer->getLastname())
                            ->setCountryId('ID')
                            ->setIsDefaultBilling('1')
                            ->setIsDefaultShipping('1')
                            ->setSaveInAddressBook('1');
                        $address->save();
                        
                        $response = array( 
                            'status_code' => '200',
                            'message_dialog' => 'success',
                            'entity_id' => $address->getId(),
                            // 'customer_id' => $address->getParentId(),
                        );
                    
                   
                    $this->_sendAPI($response);
                    }

                   
            }
            else{
                $response = array( 
                    'status_code' => '400',
                    'message_dialog' => 'method_not_allowed',
                );
            }
           
        }

        public function getCustomerByIdAction(){
            $customer_id = $this->_getCustomerId();
            $customer = Mage::getModel('customer/customer')->load($customer_id);
            if($customer_id != null){
                $customer = array( 
                    'entity_id' => $customer->getId(),
                    'email' => $customer->getEmail(),
                    'customer_bio' => $customer->getCustomerBio(),
                    'customer_photo_url' => $customer->getCustomerPhotoUrl(),
                    'firstname' => $customer->getFirstname(),
                    'lastname' => $customer->getLastname(),
                    'default_shipping' => $customer->getDefaultShipping(),
                    'website_id' => $customer->getWebsiteId(),
                );
                $response = array( 
                    'status_code' => 'allowed',
                    'message_dialog' => 'Customer ID found',
                    'customer' => $customer
                );
            }
            else{
                $response = array( 
                    'status_code' => 'not_allowed',
                    'message_dialog' => 'Customer ID not found',
                    'customer' => $customer
                );
            }

            $this->_sendAPI($response);

        }

        public function setCustomerByIdAction(){
            if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
                $customer_id = $this->_getCustomerId();
                $customer = Mage::getModel('customer/customer')->load($customer_id);
                
                $getraws = $this->getRequest()->getRawBody();
                $params = json_decode($getraws, true);
            
                if($customer->getId() ==  $customer_id){
                    $customer->setEmail($params['email']);
                    $customer->setCustomerBio($params['customer_bio']);
                    $customer->setCustomerPhotoUrl($params['customer_photo_url']);
                    $customer->setFirstname($params['firstname']);
                    $customer->setLastname($params['lastname']);
                    $customer->save();

                    $response = array( 
                        'status_code' => 'success',
                    );
                }
                else{
                    $response = array( 
                        'status_code' => 'not_allowed',
                    );
                }

            }
            else{
                $response = array( 
                    'status_code' => 'method_not_allowed',
                );
               
            }
            $this->_sendAPI($response);
        }

        public function LogoutAction(){
            try{
                        
                Mage::getSingleton('customer/session')->logout();
                $response = array(
                   
                    'message_code' => 200,
                    'message_dialog' => 'Logout Success'
                );
                                
                $this->_sendAPI($response);
                
            }catch (Exception $e){
                $response = array(
                   
                    'message_code' => 400,
                    'message_dialog' => $e
                );
                $this->_sendAPI($response);
            }   
        }
       
        public function LoginAction(){

               if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
                $getraws = $this->getRequest()->getRawBody();
                $params = json_decode($getraws, true);

                $customerEmail = $params['email'];
                $customerPassword = $params['password'];

                if (!empty($customerEmail) && !empty($customerPassword)){

                    $customer = Mage::getModel("customer/customer"); 
                    $customer->setWebsiteId(Mage::app()->getWebsite()->getId()); 
                    $customer->loadByEmail($customerEmail); 

                    //Session
                    $session = Mage::getSingleton('customer/session');
                    //$customerId = $this->_getSession($customer);
                    
                    try{
                        
                        $session->setCustomerAsLoggedIn($customer);
                        $session->login($customerEmail, $customerPassword);

                        $encrypted = Mage::helper('core')->encrypt($session->getId());

                        $apiarray = array('token' => $encrypted, 'customer_id' => $session->getId(), 
                                                'message_code' => 200,
                                                'message_dialog' => 'Login Success - Send Token');
                                        
                        $this->_sendAPI($apiarray);
                        
                    }catch (Exception $e){
                        $message_code = array('message_code' => 403, 'message_dialog' => 'Wrong Email or Password');
                        $this->_sendAPI($message_code);
                    } 
                } 
                else {
                    $message_code = array('message_code' => 403, 'message_dialog' => 'Email and Password are required');
                    $this->_sendAPI($message_code);
                }
            }
            else {
                $message_code = array('message_code' => 405, 'message_dialog' => 'Method Not Allowed');
                $this->_sendAPI($message_code);
            }
        }

        public function RegisterAction(){

            //Validation
            // $validate = Mage::helper('Rest_APIv1_helper')->AuthHeaderValidation();
    
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
                    $getraws = $this->getRequest()->getRawBody();
                    $params = json_decode($getraws, true);
    
                    $customerFirstName = $params['firstname'];
                    $customerLastName = $params['lastname'];
                    $customerEmail = $params['email'];
                    $customerPassword = $params['password'];

                    // $customerEmail = 'james@mailss.com';
                    // $customerPassword = '12345678';
    
                    if (!empty($customerEmail) && !empty($customerPassword)){
                        $customer = Mage::getModel('customer/customer');
                        $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
                        $customer->loadByEmail($customerEmail);
    
                        if(!$customer->getId()) {
                            $customer->setEmail($customerEmail);
                            $customer->setFirstname($customerFirstName);
                            $customer->setLastname($customerLastName);
                            $customer->setPassword($customerPassword);
    
                            try{
                                $customer->save();
    
                                //Session
                                $session = Mage::getSingleton('customer/session');
                                $session->start();
                                $session->setCustomerAsLoggedIn($customer);
                                $session->login($customerEmail, $customerPassword);
                                
                                $encrypted = Mage::helper('core')->encrypt($session->getId());
                                $apiarray = array('token' => $encrypted, 'customer_id' => $session->getId(), 'message_code' => 200, 'message_dialog' => 'Register Success');
                                Mage::getModel('advancednewsletter/subscriber')->subscribe($customer->getEmail(), 'new_buyer_customer','');
          
                                $this->_sendAPI($apiarray);     
                              }
                            catch (Exception $e) {
                                //Zend_Debug::dump($e->getMessage());
                                $message_code = array('message_code' => 400, 'message_dialog' => $e->getMessage());
                                $this->_sendAPI($message_code);
                            } 
                        }
                        else {
                            $message_code = array('message_code' => 400, 'message_dialog' => 'Email already exist in the system');
                            $this->_sendAPI($message_code);
                        }
                    }

                }
                else {
                    $message_code = array('message_code' => '405', 'message_dialog' => 'Method Not Allowed');
                    $this->_sendAPI($message_code);
                }
        }

        public function FacebookLoginAction(){

            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
 
             $getraws = $this->getRequest()->getRawBody();
             $params = json_decode($getraws, true);

             $token = $params['token'];

             if (!empty($token)){
                $facebook = $this->_getToken($token);
               // $this->_sendAPI($message_code);
              // $this->_sendAPI($facebook['email']);
                 $customer = Mage::getModel("customer/customer"); 
                 $customer->setWebsiteId(Mage::app()->getWebsite()->getId()); 
                 $customer->loadByEmail($facebook['email']); 

                //  check if customer is exist
                 if($customer->getId()){
                    try{
                        $session = Mage::getSingleton('customer/session');
                        $session->setCustomerAsLoggedIn($customer);
                        $encrypted = Mage::helper('core')->encrypt($session->getId());
    
                        $apiarray = array(
                            'token' => $encrypted, 
                            'customer_id' => $session->getId(), 
                            'message_code' => 200,
                            'message_dialog' => 'Login Success - Send Token');
                                        
                        $this->_sendAPI($apiarray);
                        
                    }catch (Exception $e){
                        $message_code = array('message_code' => 403, 'message_dialog' => 'Wrong Email or Password');
                        $this->_sendAPI($message_code);
                    } 
                 }

                 //create a new account 
                 else{
                    $email = $facebook['email'];
                    $fullname = $facebook['name'];
                    $firstname = $facebook['first_name'];
                    $lastname = $facebook['last_name'];
                    $this->_createNewAccount($email,$firstname,$lastname);
                 }

             } 
             else {
                 $message_code = array('message_code' => 403, 'message_dialog' => 'Token Not Found');
                 $this->_sendAPI($message_code);
             }
        }
         else {
             $message_code = array('message_code' => 405, 'message_dialog' => 'Method Not Allowed');
             $this->_sendAPI($message_code);
         }
        }

        public function _getToken($token){
             //       $token='EAAZAeJgFU6DkBABwr93kS8wPN0fD5HVhbKSRGE7emRmcS0mfwDqH4Dc7UH0TqdGagv4fCZCJ6Q8yZCIR2oNX1fHE8oZCZCh1CPnrrTsL80aTdnJonRuvtYhv1WrYBuBQ5S0tHZCZB7hmVPDyCmmLnMqcWxIf3bOrWvOj0i6hgZB5UHst9fQgLJTZChlasvMZCujsklVjleJ7iRqQZDZD';
            $url    = 'https://graph.facebook.com/v5.0/me?fields=id,name,first_name,last_name,link,birthday,gender,email,picture.type(large)&access_token='.$token;
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
            curl_setopt($curl, CURLOPT_GET, true);
            curl_setopt($curl, CURLOPT_GETFIELDS, $config);
            
            $result     = curl_exec($curl);
            $array = Mage::helper('core')->jsondecode($result);

            return $array;
        //var_dump($array);
        
        }

        public function _createNewAccount($email,$firstname,$lastname){
           
            if($email){
                try{
                    $websiteId = Mage::app()->getWebsite()->getId();
                    $store = Mage::app()->getStore();
                  
                    $customer = Mage::getModel("customer/customer");
                    $customer->setWebsiteId($websiteId)
                            ->setStore($store)
                            ->setFirstname($firstname)
                            ->setLastname($lastname)
                            ->setEmail($email)
                            ->setPassword($customer->generatePassword());
                    
                    $customer->setForceConfirmed(true);
                    $customer->save();
    
                    //Session
                    $session = Mage::getSingleton('customer/session');
                    $session->start();
                    $session->setCustomerAsLoggedIn($customer);
                    // $session->login($customerEmail, $customerPassword);

               
                    $customer->sendNewAccountEmail();
            
                    //Set password remainder email if the password is auto generated by magento
                    $customer->sendPasswordReminderEmail();
                    
                    $encrypted = Mage::helper('core')->encrypt($session->getId());
                    $apiarray = array('token' => $encrypted, 'customer_id' => $session->getId(), 'message_code' => 200, 'message_dialog' => 'Register Success');
                    
                   // Mage::getModel('advancednewsletter/subscriber')->subscribe($customer->getEmail(), 'new_buyer_customer','');
          
                    $this->_sendAPI($apiarray);
                    return true;    
                  }
                catch (Exception $e) {
                    //Zend_Debug::dump($e->getMessage());
                    $message_code = array('message_code' => 400, 'message_dialog' => $e->getMessage());
                    $this->_sendAPI($message_code);
                    return false;
                } 
            }
            

            
        }

        public function ChangePasswordAction(){

            if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                $getraws = $this->getRequest()->getRawBody();
                $params = json_decode($getraws, true);
                $customerEmail = $params['email'];
                $customerPassword = $params['password'];
                $customerNewPassword = $params['new_password'];

                $token = Mage::app()->getRequest()->getHeader('Authorization');
                $decyprted = Mage::helper('core')->decrypt($token);
                $customer = Mage::getModel('customer/customer');
                $customer->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
                $customer->loadByEmail($customerEmail);
                $customerId = $customer->getId();

                $hash = $customer->getData('password_hash');
                $hashPassword = explode(':', $hash);
                $firstPart = $hashPassword[0];
                $salt = $hashPassword[1];

                $current_password = md5($salt.$customerPassword);
                if($current_password == $firstPart){
                    if ($customerId){
                        if($customerPassword != $customerNewPassword){  
                            try{
                                $customer->setPassword($customerNewPassword);
                                $customer->save();
                                
                                $message_code = array('message_code' => 200, 'message_dialog' => 'Change Password Success');
                                $this->_sendAPI($message_code);
                            }
                            catch (Exception $e) {
                                $message_code = array('message_code' => 404, 'message_dialog' => 'Failed to change password');
                                $this->_sendAPI($message_code);
                            } 
                        }
                        else {
                            $message_code = array('message_code' => 409, 'message_dialog' => 'Change Password Error - Old and New Password are same');
                            $this->_sendAPI($message_code);
                        }
                    }
                }
                else {
                    $message_code = array('message_code' => 406, 'message_dialog' => 'Wrong Current Password');
                    $this->_sendAPI($message_code);
                }
            }
            else {
                $message_code = array('message_code' => 405, 'message_dialog' => 'Method Not Allowed');
                $this->_sendAPI($message_code);
            }
        }

        public function ForgotPasswordAction(){
            if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                $getraws = $this->getRequest()->getRawBody();
                $params = json_decode($getraws, true);
                $customerEmail = $params['email'];

                $customer = Mage::getModel("customer/customer"); 
                $customer->setWebsiteId(Mage::app()->getWebsite()->getId()); 
                $customer->loadByEmail($customerEmail); 

                $customerId = $customer->getId();
                if ($customerId) {
                    try {
                        $newResetPasswordLinkToken =  Mage::helper('customer')->generateResetPasswordLinkToken();
                        $customer->sendPasswordResetConfirmationEmail();
                        $message_code = array('message_code' => 200, 'message_dialog' => 'Reset Password Link Token has been send to your email');
                        $this->_sendAPI($message_code);
                        //echo $newResetPasswordLinkToken;
                    } catch (Exception $exception) {
                        //Mage::log($exception);
                    }
                }
                else {
                    $message_code = array('message_code' => 404, 'message_dialog' => 'Fail - Email not found');
                    $this->_sendAPI($message_code);
                }
            }
            else {
                $message_code = array('message_code' => 405, 'message_dialog' => 'Method Not Allowed');
                $this->_sendAPI($message_code);
            }
        }
        
    }

?>
