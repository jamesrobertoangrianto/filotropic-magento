<?php
/**
 * Veritrans VT Web Payment Controller
 *
 * @category   Mage
 * @package    Mage_Veritrans_snap_PaymentController
 * @author     Kisman Hong (plihplih.com), Ismail Faruqi (@ifaruqi_jpn)
 * This class is used for handle redirection after placing order.
 * function redirectAction -> redirecting to Veritrans VT Web
 * function responseAction -> when payment at Veritrans VT Web is completed or
 * failed, the page will be redirected to this function,
 * you must set this url in your Veritrans MAP merchant account.
 * http://yoursite.com/snap/payment/notification
 */

// require_once(Mage::getBaseDir('lib') . '/veritrans-php/Veritrans.php');

class Wia_Module_PaymentController
    extends Mage_Core_Controller_Front_Action {

  // /**
  //  * @return Mage_Checkout_Model_Session
  //  */
  protected function _getCheckout() {
    return Mage::getSingleton('checkout/session');
  }

  public function _sendAPI($data){

    return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
  }

  // The redirect action is triggered when someone places an order,
  // redirecting to Veritrans payment page.

  public function testAction(){
    $params = $this->getRequest()->getParams();
    $orderIncrementId = $params['order_id'];

    Mage::helper('Rest_APIv1_helper')->Payment($orderIncrementId);
  }

  public function redirectAction() {
    $params = $this->getRequest()->getParams();
    $orderIncrementId = $params['order_id'];
    
    $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
    $sessionId = Mage::getSingleton('core/session');

    $transaction_details = array();
    $transaction_details['order_id'] = $orderIncrementId;
    $transaction_details['gross_amount'] = 202200;

    $payloads = array();
    $payloads['transaction_details'] = $transaction_details;

    $config = json_encode(array(
      // "payment_type" => "bank_transfer",
      "transaction_details" => [
          "order_id" => $orderIncrementId,
          "gross_amount" => 202200
      ],
      "item_details" => [
        
        "id" => "ITEM1",
        "price" => 10000,
        "quantity" => 1,
        "name" => "Midtrans Bear",
        "brand" => "Midtrans",
        "category" => "Toys",
        "merchant_name" => "Midtrans"
      ],
      [
        "id" => "ITEM2",
        "price" => 10000,
        "quantity" => 1,
        "name" => "Midtrans Bear",
        "brand" => "Midtrans",
        "category" => "Toys",
        "merchant_name" => "Midtrans"
      ],
      "customer_details" => [
        "first_name" => "TEST",
        "last_name" => "MIDTRANSER",
        "email" => "test@midtrans.com",
        "phone" => "+628123456",
        "billing_address" => [
          "first_name" => "TEST",
          "last_name" => "MIDTRANSER",
          "email" => "test@midtrans.com",
          "phone" => "081 2233 44-55",
          "address" => "Sudirman",
          "city" => "Jakarta",
          "postal_code" => "12190",
          "country_code" => "IDN"
        ],
        "shipping_address" => [
          "first_name" => "TEST",
          "last_name" => "MIDTRANSER",
          "email" => "test@midtrans.com",
          "phone" => "0 8128-75 7-9338",
          "address" => "Sudirman",
          "city" => "Jakarta",
          "postal_code" => "12190",
          "country_code" => "IDN"
        ]
        ],
      "enabled_payments" => ["credit_card", "mandiri_clickpay", "cimb_clicks",
        "bca_klikbca", "bca_klikpay", "bri_epay", "echannel", "permata_va",
        "bca_va", "bni_va", "other_va", "gopay", "indomaret",
        "danamon_online", "akulaku"],
      "credit_card" => [
        "secure" => true,
        "channel" => "migs",
        "bank" => "bca",
        "installment" => [
          "required" => false,
          "terms" => [
            "bni" => [3, 6, 12],
            "mandiri" => [3, 6, 12],
            "cimb" => [3],
            "bca" => [3, 6, 12],
            "offline" => [6, 12]
          ]
          ],
        "whitelist_bins" => [
          "48111111",
          "41111111"
        ]
      ],
      "bca_va" => [
        "va_number" => "12345678911",
        "sub_company_code" => "00000",
        "free_text" => [
          "inquiry" => [
            [
              "en" => "text in English",
              "id" => "text in Bahasa Indonesia"
            ]
          ],
          "payment" => [
            [
              "en" => "text in English",
              "id" => "text in Bahasa Indonesia"
            ]
          ]
        ]
      ],
      "bni_va" => [
        "va_number" => "12345678"
      ],
      "permata_va" => [
        "va_number" => "1234567890",
        "recipient_name" => "SUDARSONO"
      ],
      "callbacks" => [
        "finish" => "https://demo.midtrans.com"
      ],
      "expiry" => [
        "start_time" => "2018-12-13 18:11:08 +0700",
        "unit" => "minutes",
        "duration" => 1
      ]
    ));
    
    try {

        $url    = 'https://app.sandbox.midtrans.com/snap/v1/transactions';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //WIA SANDBOX SERVER KEY
        curl_setopt($curl, CURLOPT_HTTPHEADER,array(
            "Accept: application/json",
            "Content-type: application/json",
            "Authorization: Basic ".base64_encode("SB-Mid-server-s9yxKYsHYJkETlq9iAlMin4z:")));
  
        //LOCAL SANDBOX SERVER KEY
        // curl_setopt($curl, CURLOPT_HTTPHEADER,array(
        //     "Accept: application/json",
        //     "Content-type: application/json",
        //     "Authorization: U0ItTWlkLXNlcnZlci1ld2dlcUNONENzMElOVkZJY2dDLUQ3YXQ6"));

        //WIA PRODUCTION SERVER KEY
        // curl_setopt($curl, CURLOPT_HTTPHEADER,array(
        //     "Accept: application/json",
        //     "Content-type: application/json",
        //     "Authorization: VlQtc2VydmVyLWVYRGRIZHh5UmhsR05tMW02cjFoX3hFdDo="));

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
            $result     = curl_exec($curl);
            $array = Mage::helper('core')->jsondecode($result);
            curl_close($curl);

      $this->_sendAPI($array);

      //remove item
      // foreach( Mage::getSingleton('checkout/session')->getQuote()->getItemsCollection() as $item ){
      //       Mage::getSingleton('checkout/cart')->removeItem( $item->getId() )->save();
      // }
      // Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getBaseUrl().'snapio/payment/opensnap');

    }
    catch (Exception $e) {
      // error_log($e->getMessage());
      // Mage::log('error:'.print_r($e->getMessage(),true),null,'snap.log',true);
    }
  }

  public function opensnapAction(){
      
      $template = 'snapio/open.phtml';

      //Get current layout state
      $this->loadLayout();          
      
      $block = $this->getLayout()->createBlock(
          'Mage_Core_Block_Template',
          'snap',
          array('template' => $template)
      );
     
      $this->getLayout()->getBlock('root')->setTemplate('page/empty.phtml');
      
      
      $this->getLayout()->getBlock('content')->append($block);
      
      $this->_initLayoutMessages('core/session'); 
      $this->renderLayout();
  }

  /**
   * Convert 2 digits coundry code to 3 digit country code
   *
   * @param String $country_code Country code which will be converted
   */
  public function convert_country_code( $country_code ) {

    // 3 digits country codes
    $cc_three = array(
      'AF' => 'AFG',
      'AX' => 'ALA',
      'AL' => 'ALB',
      'DZ' => 'DZA',
      'AD' => 'AND',
      'AO' => 'AGO',
      'AI' => 'AIA',
      'AQ' => 'ATA',
      'AG' => 'ATG',
      'AR' => 'ARG',
      'AM' => 'ARM',
      'AW' => 'ABW',
      'AU' => 'AUS',
      'AT' => 'AUT',
      'AZ' => 'AZE',
      'BS' => 'BHS',
      'BH' => 'BHR',
      'BD' => 'BGD',
      'BB' => 'BRB',
      'BY' => 'BLR',
      'BE' => 'BEL',
      'PW' => 'PLW',
      'BZ' => 'BLZ',
      'BJ' => 'BEN',
      'BM' => 'BMU',
      'BT' => 'BTN',
      'BO' => 'BOL',
      'BQ' => 'BES',
      'BA' => 'BIH',
      'BW' => 'BWA',
      'BV' => 'BVT',
      'BR' => 'BRA',
      'IO' => 'IOT',
      'VG' => 'VGB',
      'BN' => 'BRN',
      'BG' => 'BGR',
      'BF' => 'BFA',
      'BI' => 'BDI',
      'KH' => 'KHM',
      'CM' => 'CMR',
      'CA' => 'CAN',
      'CV' => 'CPV',
      'KY' => 'CYM',
      'CF' => 'CAF',
      'TD' => 'TCD',
      'CL' => 'CHL',
      'CN' => 'CHN',
      'CX' => 'CXR',
      'CC' => 'CCK',
      'CO' => 'COL',
      'KM' => 'COM',
      'CG' => 'COG',
      'CD' => 'COD',
      'CK' => 'COK',
      'CR' => 'CRI',
      'HR' => 'HRV',
      'CU' => 'CUB',
      'CW' => 'CUW',
      'CY' => 'CYP',
      'CZ' => 'CZE',
      'DK' => 'DNK',
      'DJ' => 'DJI',
      'DM' => 'DMA',
      'DO' => 'DOM',
      'EC' => 'ECU',
      'EG' => 'EGY',
      'SV' => 'SLV',
      'GQ' => 'GNQ',
      'ER' => 'ERI',
      'EE' => 'EST',
      'ET' => 'ETH',
      'FK' => 'FLK',
      'FO' => 'FRO',
      'FJ' => 'FJI',
      'FI' => 'FIN',
      'FR' => 'FRA',
      'GF' => 'GUF',
      'PF' => 'PYF',
      'TF' => 'ATF',
      'GA' => 'GAB',
      'GM' => 'GMB',
      'GE' => 'GEO',
      'DE' => 'DEU',
      'GH' => 'GHA',
      'GI' => 'GIB',
      'GR' => 'GRC',
      'GL' => 'GRL',
      'GD' => 'GRD',
      'GP' => 'GLP',
      'GT' => 'GTM',
      'GG' => 'GGY',
      'GN' => 'GIN',
      'GW' => 'GNB',
      'GY' => 'GUY',
      'HT' => 'HTI',
      'HM' => 'HMD',
      'HN' => 'HND',
      'HK' => 'HKG',
      'HU' => 'HUN',
      'IS' => 'ISL',
      'IN' => 'IND',
      'ID' => 'IDN',
      'IR' => 'RIN',
      'IQ' => 'IRQ',
      'IE' => 'IRL',
      'IM' => 'IMN',
      'IL' => 'ISR',
      'IT' => 'ITA',
      'CI' => 'CIV',
      'JM' => 'JAM',
      'JP' => 'JPN',
      'JE' => 'JEY',
      'JO' => 'JOR',
      'KZ' => 'KAZ',
      'KE' => 'KEN',
      'KI' => 'KIR',
      'KW' => 'KWT',
      'KG' => 'KGZ',
      'LA' => 'LAO',
      'LV' => 'LVA',
      'LB' => 'LBN',
      'LS' => 'LSO',
      'LR' => 'LBR',
      'LY' => 'LBY',
      'LI' => 'LIE',
      'LT' => 'LTU',
      'LU' => 'LUX',
      'MO' => 'MAC',
      'MK' => 'MKD',
      'MG' => 'MDG',
      'MW' => 'MWI',
      'MY' => 'MYS',
      'MV' => 'MDV',
      'ML' => 'MLI',
      'MT' => 'MLT',
      'MH' => 'MHL',
      'MQ' => 'MTQ',
      'MR' => 'MRT',
      'MU' => 'MUS',
      'YT' => 'MYT',
      'MX' => 'MEX',
      'FM' => 'FSM',
      'MD' => 'MDA',
      'MC' => 'MCO',
      'MN' => 'MNG',
      'ME' => 'MNE',
      'MS' => 'MSR',
      'MA' => 'MAR',
      'MZ' => 'MOZ',
      'MM' => 'MMR',
      'NA' => 'NAM',
      'NR' => 'NRU',
      'NP' => 'NPL',
      'NL' => 'NLD',
      'AN' => 'ANT',
      'NC' => 'NCL',
      'NZ' => 'NZL',
      'NI' => 'NIC',
      'NE' => 'NER',
      'NG' => 'NGA',
      'NU' => 'NIU',
      'NF' => 'NFK',
      'KP' => 'MNP',
      'NO' => 'NOR',
      'OM' => 'OMN',
      'PK' => 'PAK',
      'PS' => 'PSE',
      'PA' => 'PAN',
      'PG' => 'PNG',
      'PY' => 'PRY',
      'PE' => 'PER',
      'PH' => 'PHL',
      'PN' => 'PCN',
      'PL' => 'POL',
      'PT' => 'PRT',
      'QA' => 'QAT',
      'RE' => 'REU',
      'RO' => 'SHN',
      'RU' => 'RUS',
      'RW' => 'EWA',
      'BL' => 'BLM',
      'SH' => 'SHN',
      'KN' => 'KNA',
      'LC' => 'LCA',
      'MF' => 'MAF',
      'SX' => 'SXM',
      'PM' => 'SPM',
      'VC' => 'VCT',
      'SM' => 'SMR',
      'ST' => 'STP',
      'SA' => 'SAU',
      'SN' => 'SEN',
      'RS' => 'SRB',
      'SC' => 'SYC',
      'SL' => 'SLE',
      'SG' => 'SGP',
      'SK' => 'SVK',
      'SI' => 'SVN',
      'SB' => 'SLB',
      'SO' => 'SOM',
      'ZA' => 'ZAF',
      'GS' => 'SGS',
      'KR' => 'KOR',
      'SS' => 'SSD',
      'ES' => 'ESP',
      'LK' => 'LKA',
      'SD' => 'SDN',
      'SR' => 'SUR',
      'SJ' => 'SJM',
      'SZ' => 'SWZ',
      'SE' => 'SWE',
      'CH' => 'CHE',
      'SY' => 'SYR',
      'TW' => 'TWN',
      'TJ' => 'TJK',
      'TZ' => 'TZA',
      'TH' => 'THA',
      'TL' => 'TLS',
      'TG' => 'TGO',
      'TK' => 'TKL',
      'TO' => 'TON',
      'TT' => 'TTO',
      'TN' => 'TUN',
      'TR' => 'TUR',
      'TM' => 'TKM',
      'TC' => 'TCA',
      'TV' => 'TUV',
      'UG' => 'UGA',
      'UA' => 'UKR',
      'AE' => 'ARE',
      'GB' => 'GBR',
      'US' => 'USA',
      'UY' => 'URY',
      'UZ' => 'UZB',
      'VU' => 'VUT',
      'VA' => 'VAT',
      'VE' => 'VEN',
      'VN' => 'VNM',
      'WF' => 'WLF',
      'EH' => 'ESH',
      'WS' => 'WSM',
      'YE' => 'YEM',
      'ZM' => 'ZMB',
      'ZW' => 'ZWE'
    );

    // Check if country code exists
    if( isset( $cc_three[ $country_code ] ) && $cc_three[ $country_code ] != '' ) {
      $country_code = $cc_three[ $country_code ];
    }

    return $country_code;
  }
}

?>